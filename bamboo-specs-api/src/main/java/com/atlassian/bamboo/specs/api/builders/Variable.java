package com.atlassian.bamboo.specs.api.builders;

import com.atlassian.bamboo.specs.api.model.VariableProperties;
import com.atlassian.bamboo.specs.api.validators.common.ImporterUtils;
import org.jetbrains.annotations.NotNull;

/**
 * Represents a Bamboo variable.
 */
public class Variable extends EntityPropertiesBuilder<VariableProperties> {
    private String name;
    private String value;

    /**
     * Specifies a variable with given name and value.
     *
     * @param name  name of the variable
     * @param value value of the variable
     */
    public Variable(@NotNull String name, @NotNull String value) {
        ImporterUtils.checkNotNull("name", name);
        ImporterUtils.checkNotNull("value", value);
        this.name = name;
        this.value = value;
    }

    /**
     * Sets variable name.
     * Variables with names containing words like 'password', 'secret', 'passwd' are considered 'secret' and are encrypted
     * when stored by Bamboo.
     */
    public Variable name(@NotNull String name) {
        ImporterUtils.checkNotNull("name", name);
        this.name = name;
        return this;
    }

    /**
     * Sets variable value.
     * In case of 'secret' variables, both encrypted and unencrypted form is valid.
     */
    public Variable value(@NotNull String value) {
        ImporterUtils.checkNotNull("value", value);
        this.value = value;
        return this;
    }

    protected VariableProperties build() {
        return new VariableProperties(name, value);
    }
}
