/**
 * Shared credentials for authenticating repositories or AWS access.
 */
package com.atlassian.bamboo.specs.api.builders.credentials;
