package com.atlassian.bamboo.specs.api.builders.deployment;

import com.atlassian.bamboo.specs.api.builders.Applicability;
import com.atlassian.bamboo.specs.api.builders.EntityPropertiesBuilder;
import com.atlassian.bamboo.specs.api.builders.Variable;
import com.atlassian.bamboo.specs.api.builders.notification.Notification;
import com.atlassian.bamboo.specs.api.builders.requirement.Requirement;
import com.atlassian.bamboo.specs.api.builders.task.Task;
import com.atlassian.bamboo.specs.api.builders.trigger.Trigger;
import com.atlassian.bamboo.specs.api.exceptions.PropertiesValidationException;
import com.atlassian.bamboo.specs.api.model.VariableProperties;
import com.atlassian.bamboo.specs.api.model.deployment.EnvironmentProperties;
import com.atlassian.bamboo.specs.api.model.notification.NotificationProperties;
import com.atlassian.bamboo.specs.api.model.notification.NotificationRecipientProperties;
import com.atlassian.bamboo.specs.api.model.plan.requirement.RequirementProperties;
import com.atlassian.bamboo.specs.api.model.task.TaskProperties;
import com.atlassian.bamboo.specs.api.model.trigger.TriggerProperties;
import com.atlassian.bamboo.specs.api.util.EntityPropertiesBuilders;
import com.atlassian.bamboo.specs.api.validators.common.ImporterUtils;
import com.atlassian.bamboo.specs.api.validators.common.ValidationProblem;
import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static com.atlassian.bamboo.specs.api.validators.common.ImporterUtils.checkNotNull;

/**
 * Represents a deployment environment.
 */
public class Environment extends EntityPropertiesBuilder<EnvironmentProperties> {

    private String name;
    private String description;
    private List<TaskProperties> tasks = new ArrayList<>();
    private List<TriggerProperties> triggers = new ArrayList<>();
    private List<VariableProperties> variables = new ArrayList<>();
    private List<RequirementProperties> requirements = new ArrayList<>();
    private List<NotificationProperties> notifications = new ArrayList<>();

    /**
     * Specifies environment.
     *
     * @param name name of the environment.
     */
    public Environment(@NotNull String name) {
        ImporterUtils.checkNotNull("name", name);
        this.name = name;
    }

    /**
     * Sets the environment description.
     */
    public Environment description(String description) {
        this.description = description;
        return this;
    }

    /**
     * Adds provided {@link Task}s to the list of tasks executed by the environment.
     */
    public Environment tasks(@NotNull Task<?, ?>... tasks) {
        checkNotNull("tasks", tasks);
        for (Task t : tasks) {
            TaskProperties task = EntityPropertiesBuilders.build(t);
            if (!task.applicableTo().contains(Applicability.DEPLOYMENTS)) {
                throw new PropertiesValidationException("Task " + t.getClass().getSimpleName() + " is not available in deployments");
            } else {
                this.tasks.add(task);
            }
        }
        return this;
    }

    /**
     * Adds triggers to this environment.
     */
    public Environment triggers(@NotNull Trigger<?, ?>... triggers) {
        checkNotNull("triggers", triggers);
        for (Trigger t : triggers) {
            TriggerProperties trigger = EntityPropertiesBuilders.build(t);
            if (!trigger.applicableTo().contains(Applicability.DEPLOYMENTS)) {
                throw new PropertiesValidationException("Trigger " + t.getClass().getSimpleName() + " is not available in deployments");
            } else {
                this.triggers.add(trigger);
            }
        }
        return this;
    }

    /**
     * Adds deployment environment variables.
     */
    public Environment variables(@NotNull Variable... variables) {
        checkNotNull("variables", variables);
        Arrays.stream(variables)
                .map(EntityPropertiesBuilders::build)
                .forEach(this.variables::add);
        return this;
    }

    /**
     * Adds custom requirements to the environment.
     * <p>
     * Requirements control which Bamboo agents are able to execute deployments to the environment.
     */
    public Environment requirements(Requirement... requirements) {
        checkNotNull("requirements", requirements);
        Arrays.stream(requirements)
                .map(EntityPropertiesBuilders::build)
                .forEach(this.requirements::add);
        return this;
    }

    /**
     * Appends a notification rule to the environment.
     */
    public Environment notifications(@NotNull final Notification... notifications) {
        ImporterUtils.checkNotNull("notifications", notifications);
        Arrays.stream(notifications)
                .map(EntityPropertiesBuilders::build)
                .forEach(notification -> {
                    List<ValidationProblem> problems = new ArrayList<>();
                    if (!notification.getType().applicableTo().contains(Applicability.DEPLOYMENTS)) {
                        problems.add(new ValidationProblem(String.format("Can't add notification because notification type (%s) is not available in deployments",
                                notification.getType().getAtlassianPlugin().getCompleteModuleKey())));
                    }

                    for (NotificationRecipientProperties recipient : notification.getRecipients()) {
                        if (!recipient.applicableTo().contains(Applicability.DEPLOYMENTS)) {
                            problems.add(new ValidationProblem(String.format("Can't add notification because notification recipient (%s) is not available in deployments",
                                    recipient.getAtlassianPlugin().getCompleteModuleKey())));
                        }
                    }

                    if (!problems.isEmpty()) {
                        throw new PropertiesValidationException(problems);
                    } else {
                        this.notifications.add(notification);
                    }
                });
        return this;
    }

    @Override
    protected EnvironmentProperties build() {
        return new EnvironmentProperties(name, description, tasks, triggers, variables, requirements, notifications);
    }
}
