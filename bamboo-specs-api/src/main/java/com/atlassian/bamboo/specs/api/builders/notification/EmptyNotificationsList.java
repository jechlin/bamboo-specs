package com.atlassian.bamboo.specs.api.builders.notification;

import com.atlassian.bamboo.specs.api.model.notification.EmptyNotificationsListProperties;
import org.jetbrains.annotations.NotNull;

@Deprecated
public class EmptyNotificationsList extends Notification {
    @Override
    public Notification type(@NotNull NotificationType type) {
        throw new RuntimeException("type can't be set on EmptyNotificationsList");
    }

    @Override
    public Notification recipients(@NotNull NotificationRecipient... recipients) {
        throw new RuntimeException("type can't be set on EmptyNotificationsList");
    }

    @Override
    protected EmptyNotificationsListProperties build() {
        return new EmptyNotificationsListProperties();
    }
}
