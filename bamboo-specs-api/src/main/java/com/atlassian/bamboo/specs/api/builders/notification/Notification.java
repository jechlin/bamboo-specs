package com.atlassian.bamboo.specs.api.builders.notification;

import com.atlassian.bamboo.specs.api.builders.EntityPropertiesBuilder;
import com.atlassian.bamboo.specs.api.model.notification.NotificationProperties;
import com.atlassian.bamboo.specs.api.model.notification.NotificationRecipientProperties;
import com.atlassian.bamboo.specs.api.model.notification.NotificationTypeProperties;
import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static com.atlassian.bamboo.specs.api.validators.common.ImporterUtils.checkNotNull;

/**
 * Represents a notification configuration.
 */
public class Notification extends EntityPropertiesBuilder<NotificationProperties> {

    private NotificationTypeProperties type;
    private List<NotificationRecipientProperties> recipients = new ArrayList<>();

    /**
     * Defines a type of the notifications.
     */
    public Notification type(@NotNull NotificationType type) {
        this.type = type.build();
        return this;
    }

    /**
     * Defines notification recipient.
     */
    public Notification recipients(@NotNull NotificationRecipient... recipients) {
        checkNotNull("recipients", recipients);
        Arrays.stream(recipients)
                .map(NotificationRecipient::build)
                .forEach(this.recipients::add);
        return this;
    }

    protected NotificationProperties build() {
        return new NotificationProperties(type, recipients);
    }
}
