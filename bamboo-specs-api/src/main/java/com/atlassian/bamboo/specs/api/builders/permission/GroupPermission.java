package com.atlassian.bamboo.specs.api.builders.permission;

import com.atlassian.bamboo.specs.api.builders.EntityPropertiesBuilder;
import com.atlassian.bamboo.specs.api.model.permission.GroupPermissionProperties;

import java.util.Arrays;
import java.util.LinkedHashSet;
import java.util.Set;

public class GroupPermission extends EntityPropertiesBuilder<GroupPermissionProperties> {

    private String group;
    private final Set<PermissionType> permissionTypes = new LinkedHashSet<>();

    public GroupPermission(final String group) {
        this.group = group;
    }

    public GroupPermission permissions(final PermissionType... permissionTypes) {
        Arrays.stream(permissionTypes).forEach(this.permissionTypes::add);
        return this;
    }

    @Override
    protected GroupPermissionProperties build() {
        return new GroupPermissionProperties(group, permissionTypes);
    }
}
