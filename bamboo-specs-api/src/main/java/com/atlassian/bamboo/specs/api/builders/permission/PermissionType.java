package com.atlassian.bamboo.specs.api.builders.permission;

public enum PermissionType {
    VIEW,
    EDIT,
    DELETE,
    BUILD,
    CLONE,
    ADMIN
}
