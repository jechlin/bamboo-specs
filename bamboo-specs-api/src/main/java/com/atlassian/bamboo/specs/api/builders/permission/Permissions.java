package com.atlassian.bamboo.specs.api.builders.permission;

import com.atlassian.bamboo.specs.api.builders.EntityPropertiesBuilder;
import com.atlassian.bamboo.specs.api.model.permission.PermissionsProperties;

import java.util.LinkedHashMap;
import java.util.Map;

/**
 * Entity representing permissions. Lists all permissions on related entity that are granted to specified identities.
 * Note that all pre-existing permissions not listed in this object are revoked.
 *
 * See {@link PlanPermissions}.
 * See {@link DeploymentPermissions}.
 * See {@link EnvironmentPermissions}.
 */
public class Permissions extends EntityPropertiesBuilder<PermissionsProperties> {

    private Map<String, UserPermission> userPermissions = new LinkedHashMap<>();
    private Map<String, GroupPermission> groupPermissions = new LinkedHashMap<>();
    private LoggedInUserPermissions loggedInUserPermissions = new LoggedInUserPermissions();
    private AnonymousUserPermissions anonymousUserPermissions = new AnonymousUserPermissions();

    public Permissions userPermissions(final String user, final PermissionType... permissionTypes) {
        userPermissions.putIfAbsent(user, new UserPermission(user));
        userPermissions.get(user).permissions(permissionTypes);
        return this;
    }

    public Permissions groupPermissions(final String group, final PermissionType... permissionTypes) {
        groupPermissions.putIfAbsent(group, new GroupPermission(group));
        groupPermissions.get(group).permissions(permissionTypes);
        return this;
    }

    public Permissions loggedInUserPermissions(final PermissionType... permissionTypes) {
        loggedInUserPermissions.permissions(permissionTypes);
        return this;
    }

    public Permissions anonymousUserPermissionView() {
        anonymousUserPermissions.view();
        return this;
    }

    @Override
    protected PermissionsProperties build() {
        return new PermissionsProperties(
                userPermissions.values(),
                groupPermissions.values(),
                loggedInUserPermissions,
                anonymousUserPermissions
        );
    }
}
