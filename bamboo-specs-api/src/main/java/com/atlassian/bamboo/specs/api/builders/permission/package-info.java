/**
 * Permissions like plan permissions, deployment permissions etc.
 */
package com.atlassian.bamboo.specs.api.builders.permission;
