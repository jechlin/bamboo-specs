package com.atlassian.bamboo.specs.api.builders.plan;

import com.atlassian.bamboo.specs.api.builders.Applicability;
import com.atlassian.bamboo.specs.api.builders.BambooKey;
import com.atlassian.bamboo.specs.api.builders.BambooOid;
import com.atlassian.bamboo.specs.api.builders.RootEntityPropertiesBuilder;
import com.atlassian.bamboo.specs.api.builders.Variable;
import com.atlassian.bamboo.specs.api.builders.notification.EmptyNotificationsList;
import com.atlassian.bamboo.specs.api.builders.notification.Notification;
import com.atlassian.bamboo.specs.api.builders.plan.branches.PlanBranchManagement;
import com.atlassian.bamboo.specs.api.builders.plan.configuration.AllOtherPluginsConfiguration;
import com.atlassian.bamboo.specs.api.builders.plan.configuration.PluginConfiguration;
import com.atlassian.bamboo.specs.api.builders.plan.dependencies.Dependencies;
import com.atlassian.bamboo.specs.api.builders.plan.dependencies.EmptyDependenciesList;
import com.atlassian.bamboo.specs.api.builders.project.Project;
import com.atlassian.bamboo.specs.api.builders.repository.PlanRepositoryLink;
import com.atlassian.bamboo.specs.api.builders.repository.VcsRepository;
import com.atlassian.bamboo.specs.api.builders.repository.VcsRepositoryIdentifier;
import com.atlassian.bamboo.specs.api.builders.trigger.Trigger;
import com.atlassian.bamboo.specs.api.exceptions.PropertiesValidationException;
import com.atlassian.bamboo.specs.api.model.BambooKeyProperties;
import com.atlassian.bamboo.specs.api.model.BambooOidProperties;
import com.atlassian.bamboo.specs.api.model.VariableProperties;
import com.atlassian.bamboo.specs.api.model.notification.EmptyNotificationsListProperties;
import com.atlassian.bamboo.specs.api.model.notification.NotificationProperties;
import com.atlassian.bamboo.specs.api.model.notification.NotificationRecipientProperties;
import com.atlassian.bamboo.specs.api.model.plan.PlanProperties;
import com.atlassian.bamboo.specs.api.model.plan.StageProperties;
import com.atlassian.bamboo.specs.api.model.plan.branches.PlanBranchManagementProperties;
import com.atlassian.bamboo.specs.api.model.plan.configuration.PluginConfigurationProperties;
import com.atlassian.bamboo.specs.api.model.plan.dependencies.DependenciesProperties;
import com.atlassian.bamboo.specs.api.model.project.ProjectProperties;
import com.atlassian.bamboo.specs.api.model.repository.PlanRepositoryLinkProperties;
import com.atlassian.bamboo.specs.api.model.trigger.TriggerProperties;
import com.atlassian.bamboo.specs.api.rsbs.RepositoryStoredSpecsData;
import com.atlassian.bamboo.specs.api.rsbs.RunnerSettings;
import com.atlassian.bamboo.specs.api.util.EntityPropertiesBuilders;
import com.atlassian.bamboo.specs.api.validators.common.ImporterUtils;
import com.atlassian.bamboo.specs.api.validators.common.ValidationProblem;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import static com.atlassian.bamboo.specs.api.validators.common.ImporterUtils.checkNotBlank;
import static com.atlassian.bamboo.specs.api.validators.common.ImporterUtils.checkNotNull;

/**
 * Represents a Bamboo plan.
 */
public class Plan extends RootEntityPropertiesBuilder<PlanProperties> {
    private BambooKeyProperties key;
    private BambooOidProperties oid;
    private String name;
    private String description = "";
    private ProjectProperties project;
    private List<StageProperties> stages = new ArrayList<>();
    private List<PlanRepositoryLinkProperties> repositories = new ArrayList<>();
    private List<TriggerProperties> triggers = new ArrayList<>();
    private List<VariableProperties> variables = new ArrayList<>();
    private boolean enabled = true;
    private PlanBranchManagementProperties planBranchManagement;
    private DependenciesProperties dependencies;
    private Map<String, PluginConfigurationProperties> pluginConfigurations = new LinkedHashMap<>();
    private List<NotificationProperties> notifications = new ArrayList<>();

    public Plan(@NotNull final Plan copy) {
        this.key = copy.key;
        this.oid = copy.oid;
        this.name = copy.name;
        this.description = copy.description;
        this.project = copy.project;
        this.stages.addAll(copy.stages);
        this.repositories.addAll(copy.repositories);
        this.triggers.addAll(copy.triggers);
        this.variables.addAll(copy.variables);
        this.enabled = copy.enabled;
        this.planBranchManagement = copy.planBranchManagement;
        this.dependencies = copy.dependencies;
        this.pluginConfigurations.putAll(copy.pluginConfigurations);
        this.notifications.addAll(copy.notifications);
    }

    /**
     * Specify plan with given project, name and key.
     * <p>
     * If oid is not specified, key serves as a plan identifier.
     * If a plan with specified key does not exist, a new one is created, otherwise it is updated.
     *
     * @param project project this plan belongs to. Project must exist.
     * @param name    plan's name
     * @param key     plan's short key, must be unique within the project.
     * @see #oid(String)
     */
    public Plan(@NotNull Project project, @NotNull String name, @NotNull String key) throws PropertiesValidationException {
        this(project, name, key != null ? new BambooKey(key) : null);
    }

    /**
     * Specify plan with given project, name and key.
     * <p>
     * If oid is not specified, key serves as a plan identifier.
     * If a plan with specified key does not exist, a new one is created, otherwise it is updated.
     *
     * @param project project this plan belongs to. Project must exist.
     * @param name    plan's name
     * @param key     plan's short key, must be unique within the project.
     * @see #oid(String)
     */
    public Plan(@NotNull Project project, @NotNull String name, @NotNull BambooKey key) throws PropertiesValidationException {
        checkNotNull("project", project);
        checkNotNull("name", name);
        checkNotNull("key", key);
        this.project = EntityPropertiesBuilders.build(project);
        this.name = name;
        this.key = EntityPropertiesBuilders.build(key);

        // setup defaults
        planBranchManagement(new PlanBranchManagement());
        dependencies(new Dependencies());
    }

    /**
     * Sets the project plan belongs to. Project must exist.
     */
    public Plan inProject(@NotNull Project project) throws PropertiesValidationException {
        checkNotNull("project", project);
        this.project = EntityPropertiesBuilders.build(project);
        return this;
    }

    /**
     * Sets the plan name.
     */
    public Plan name(@NotNull String name) throws PropertiesValidationException {
        checkNotBlank("name", name);
        this.name = name;
        return this;
    }

    /**
     * Sets the plan's key.
     * <p>
     * If oid is not specified, key serves as a plan identifier.
     * If a plan with specified key does not exist, a new one is created, otherwise it is updated.
     *
     * @param key plan's short key, must be unique within the project.
     * @see #oid(String)
     */
    public Plan key(@Nullable String key) throws PropertiesValidationException {
        return key(key != null ? new BambooKey(key) : null);
    }

    /**
     * Sets the plan's key.
     * <p>
     * If oid is not specified, key serves as a plan identifier.
     * If a plan with specified key does not exist, a new one is created, otherwise it is updated.
     *
     * @param key plan's short key, must be unique within the project.
     * @see #oid(String)
     */
    public Plan key(@Nullable BambooKey key) throws PropertiesValidationException {
        this.key = key != null ? EntityPropertiesBuilders.build(key) : null;
        return this;
    }

    /**
     * Sets the plans's oid.
     * <p>
     * If set, it is used to identify the plan.
     * If a plan with specified oid does not exist, a new one is created, otherwise it is updated.
     */
    public Plan oid(@Nullable String oid) throws PropertiesValidationException {
        return oid(oid != null ? new BambooOid(oid) : null);
    }

    /**
     * Sets the plans's oid.
     * <p>
     * If set, it is used to identify the plan.
     * If a plan with specified oid does not exist, a new one is created, otherwise it is updated.
     */
    public Plan oid(@Nullable BambooOid oid) throws PropertiesValidationException {
        this.oid = oid != null ? EntityPropertiesBuilders.build(oid) : null;
        return this;
    }

    /**
     * Sets the plan's description.
     */
    public Plan description(@Nullable String description) throws PropertiesValidationException {
        this.description = description;
        return this;
    }

    /**
     * Adds the stages to the end of stages list. The order of stages is preserved.
     */
    public Plan stages(@NotNull Stage... stages) {
        checkNotNull("stages", stages);
        Arrays.stream(stages)
                .map(EntityPropertiesBuilders::build)
                .forEach(this.stages::add);
        return this;
    }

    /**
     * Adds local repositories to this plan.
     * <p>
     * This method will create new plan-local repository definitions for this plan. To link plan to global
     * repositories instead, use {@link #linkedRepositories(String[])}.
     */
    public Plan planRepositories(@NotNull VcsRepository... repositories) {
        checkNotNull("repositories", repositories);
        Arrays.stream(repositories)
                .map(repo -> new PlanRepositoryLink().localRepositoryDefinition(repo))
                .map(EntityPropertiesBuilders::build)
                .forEach(this.repositories::add);
        return this;
    }

    /**
     * Adds linked repositories with given names to this plan.
     * <p>
     * This method will link existing global repositories to this plan. To add plan-local repository definitions
     * instead, use {@link #planRepositories(VcsRepository[])}.
     */
    public Plan linkedRepositories(@NotNull String... repositoriesNames) {
        checkNotNull("repositoriesNames", repositoriesNames);
        Arrays.stream(repositoriesNames)
                .map(VcsRepositoryIdentifier::new)
                .map(PlanRepositoryLink::linkToGlobalRepository)
                .map(EntityPropertiesBuilders::build)
                .forEach(this.repositories::add);
        return this;
    }

    /**
     * Adds linked repositories to this plan.
     * <p>
     * This method will link existing global repositories to this plan. To add plan-local repository definitions
     * instead, use {@link #planRepositories(VcsRepository[])}.
     */
    public Plan linkedRepositories(@NotNull VcsRepositoryIdentifier... repositories) {
        checkNotNull("repositories", repositories);
        Arrays.stream(repositories)
                .map(PlanRepositoryLink::linkToGlobalRepository)
                .map(EntityPropertiesBuilders::build)
                .forEach(this.repositories::add);
        return this;
    }

    /**
     * Adds triggers to this plan.
     */
    public Plan triggers(@NotNull Trigger<?, ?>... triggers) {
        checkNotNull("triggers", triggers);
        for (Trigger t : triggers) {
            TriggerProperties trigger = EntityPropertiesBuilders.build(t);
            if (!trigger.applicableTo().contains(Applicability.PLANS)) {
                throw new PropertiesValidationException("Trigger " + t.getClass().getSimpleName() + " is not available in plans");
            } else {
                this.triggers.add(trigger);
            }
        }
        return this;
    }

    /**
     * Adds plan variables.
     */
    public Plan variables(@NotNull Variable... variables) {
        checkNotNull("variables", variables);
        Arrays.stream(variables)
                .map(EntityPropertiesBuilders::build)
                .forEach(this.variables::add);
        return this;
    }

    /**
     * Enables/disables the plan.
     */
    public Plan enabled(boolean enabled) throws PropertiesValidationException {
        this.enabled = enabled;
        return this;
    }

    /**
     * Specifies plan branch management options for this plan. These options control if and how Bamboo should perform automatic branch management.
     *
     * @see PlanBranchManagement
     */
    public Plan planBranchManagement(@NotNull PlanBranchManagement planBranchManagement) throws PropertiesValidationException {
        checkNotNull("planBranchManagement", planBranchManagement);
        this.planBranchManagement = EntityPropertiesBuilders.build(planBranchManagement);
        return this;
    }

    /**
     * Specifies configuration of dependencies support for this plan.
     *
     * @see Dependencies
     */
    public Plan dependencies(@NotNull Dependencies dependencies) throws PropertiesValidationException {
        checkNotNull("dependencies", dependencies);
        this.dependencies = EntityPropertiesBuilders.build(dependencies);
        return this;
    }

    /**
     * Use this method to specify that your plan has no dependencies on other plans.
     * If you have dependencies on other plans specified in Bamboo and you use this method in Bamboo Specs,
     * your existing plan dependencies will be cleaned.
     */
    @Deprecated
    public Plan noDependencies() {
        return dependencies(new EmptyDependenciesList());
    }

    /**
     * Appends plugin configuration to the plan. If the same plugin is specified second time, its configuration
     * is overwritten.
     */
    public Plan pluginConfigurations(@NotNull final PluginConfiguration... pluginConfigurations) {
        ImporterUtils.checkNotNull("pluginConfigurations", pluginConfigurations);
        for (PluginConfiguration pluginConfiguration : pluginConfigurations) {
            if (pluginConfiguration != null) {
                putPluginConfiguration(pluginConfiguration);
            }
        }
        return this;
    }

    /**
     * Use this method to specify that your plan has default plugins configuration.
     */
    @Deprecated
    public Plan noPluginConfigurations() {
        return pluginConfigurations(new AllOtherPluginsConfiguration());
    }

    private void putPluginConfiguration(final PluginConfiguration pluginConfiguration) {
        PluginConfigurationProperties pluginConfigurationProperties = EntityPropertiesBuilders.build(pluginConfiguration);
        pluginConfigurations.put(pluginConfigurationProperties.getAtlassianPlugin().getCompleteModuleKey(), pluginConfigurationProperties);
    }

    /**
     * Appends a notification rule to the plan.
     */
    public Plan notifications(@NotNull final Notification... notifications) {
        ImporterUtils.checkNotNull("notifications", notifications);
        Arrays.stream(notifications)
                .map(EntityPropertiesBuilders::build)
                .forEach(notification -> {
                    List<ValidationProblem> problems = new ArrayList<>();

                    if (!(notification instanceof EmptyNotificationsListProperties)) {
                        if (!notification.getType().applicableTo().contains(Applicability.PLANS)) {
                            problems.add(new ValidationProblem(String.format("Can't add notification because notification type (%s) is not available in plans",
                                    notification.getType().getAtlassianPlugin().getCompleteModuleKey())));
                        }

                        for (NotificationRecipientProperties recipient : notification.getRecipients()) {
                            if (!recipient.applicableTo().contains(Applicability.PLANS)) {
                                problems.add(new ValidationProblem(String.format("Can't add notification because notification recipient (%s) is not available in plans",
                                        recipient.getAtlassianPlugin().getCompleteModuleKey())));
                            }
                        }
                    }
                    if (!problems.isEmpty()) {
                        throw new PropertiesValidationException(problems);
                    } else {
                        this.notifications.add(notification);
                    }
                });
        return this;
    }

    /**
     * Use this method to specify that your plan has no notification rules.
     * If you have notification rules specified in Bamboo and you use this method in Bamboo Specs,
     * your existing plan notification rules will be cleaned.
     */
    @Deprecated
    public Plan noNotifications() {
        return notifications(new EmptyNotificationsList());
    }

    /**
     * Returns identifier for this object.
     *
     * @throws IllegalStateException if key is undefined
     */
    @NotNull
    public PlanIdentifier getIdentifier() {
        if (oid != null) {
            PlanIdentifier planIdentifier = new PlanIdentifier(new BambooOid(oid.getOid()));
            if (project != null) {
                planIdentifier.projectKey(project.getKey().getKey());
            }
            if (key != null) {
                planIdentifier.key(key.getKey());
            }
            return planIdentifier;
        } else if (project != null && key != null) {
            PlanIdentifier planIdentifier = new PlanIdentifier(project.getKey().getKey(), key.getKey());
            return planIdentifier;
        }
        throw new IllegalStateException("Plan identifier is undefined");
    }

    /**
     * Returns plan's key.
     *
     * @throws IllegalStateException if key is undefined
     */
    @NotNull
    public BambooKey getKey() {
        if (key == null) {
            throw new IllegalStateException("Plan key is undefined");
        }
        return new BambooKey(key.getKey());
    }

    /**
     * Returns plan's oid.
     *
     * @throws IllegalStateException if oid is undefined
     */
    @NotNull
    public BambooOid getOid() {
        if (oid == null) {
            throw new IllegalStateException("Plan oid is undefined");
        }
        return new BambooOid(oid.getOid());
    }

    protected PlanProperties build() throws PropertiesValidationException {
        final RepositoryStoredSpecsData repositoryStoredSpecsData = RunnerSettings.getRepositoryStoredSpecsData();
        return new PlanProperties(
                oid,
                key,
                name,
                description,
                project,
                stages,
                repositories,
                triggers,
                variables,
                enabled,
                pluginConfigurations.values(),
                planBranchManagement,
                dependencies,
                repositoryStoredSpecsData,
                notifications);
    }

    @Override
    public String humanReadableId() {
        if (key != null) {
            if (project != null && project.getKey() != null) {
                return String.format("plan %s-%s", project.getKey().getKey(), key.getKey());
            } else {
                return String.format("plan %s", key.getKey());
            }
        } else {
            return "plan <unknown>";
        }
    }
}
