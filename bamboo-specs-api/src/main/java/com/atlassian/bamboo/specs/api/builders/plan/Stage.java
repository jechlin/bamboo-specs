package com.atlassian.bamboo.specs.api.builders.plan;

import com.atlassian.bamboo.specs.api.builders.EntityPropertiesBuilder;
import com.atlassian.bamboo.specs.api.exceptions.PropertiesValidationException;
import com.atlassian.bamboo.specs.api.model.plan.JobProperties;
import com.atlassian.bamboo.specs.api.model.plan.StageProperties;
import com.atlassian.bamboo.specs.api.util.EntityPropertiesBuilders;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static com.atlassian.bamboo.specs.api.validators.common.ImporterUtils.checkNotNull;

/**
 * Represents a stage within Bamboo plan.
 * <p>
 * Stages organize plan into parts that are executed sequentially.
 */
public class Stage extends EntityPropertiesBuilder<StageProperties> {
    private String name;
    private String description = "";
    private boolean manualStage;
    private boolean finalStage;
    private List<JobProperties> jobs = new ArrayList<>();

    public Stage(@NotNull final Stage copy) {
        this.name = copy.name;
        this.description = copy.description;
        this.manualStage = copy.manualStage;
        this.finalStage = copy.finalStage;
        this.jobs.addAll(copy.jobs);
    }

    /**
     * Specify a stage with a given name.
     * In the absence of oid, the name is used to identify the stage.
     * If the stage with given name does not exist, a new one is created, otherwise existing one is updated.
     * <p>
     * The name should be unique in scope of a plan it is defined in.
     */
    public Stage(@NotNull String name) throws PropertiesValidationException {
        checkNotNull("name", name);
        this.name = name;
    }

    /**
     * Sets a stage's name.
     * In the absence of oid, the name is used to identify the stage.
     * If the stage with given name does not exist, a new one is created, otherwise existing one is updated.
     * <p>
     * The name should be unique in scope of a plan it is defined in.
     */
    public Stage name(@NotNull String name) throws PropertiesValidationException {
        checkNotNull("name", name);
        this.name = name;
        return this;
    }

    /**
     * Sets a stage description.
     */
    public Stage description(@Nullable String description) throws PropertiesValidationException {
        this.description = description;
        return this;
    }

    /**
     * Specifies if stage is a manual stage.
     * <p>
     * If stage is a manual stage, Bamboo waits for user confirmation before starting it.
     */
    public Stage manual(boolean isManual) throws PropertiesValidationException {
        this.manualStage = isManual;
        return this;
    }

    /**
     * Specifies if stage is a final stage.
     * <p>
     * If stage is a final stage, it will be executed even if previous stages failed. Note however that placing manual stage
     * before a final stage will prevent the final stage from executing until a user confirms execution of the manual stage.
     */
    public Stage finalStage(boolean isFinal) throws PropertiesValidationException {
        this.finalStage = isFinal;
        return this;
    }

    /**
     * Adds {@link Job}s to the stage.
     */
    public Stage jobs(@NotNull Job... jobs) {
        checkNotNull("jobs", jobs);
        Arrays.stream(jobs)
                .map(EntityPropertiesBuilders::build)
                .forEach(this.jobs::add);
        return this;
    }

    protected StageProperties build() throws PropertiesValidationException {
        return new StageProperties(
                name,
                description,
                manualStage,
                finalStage,
                jobs);
    }
}
