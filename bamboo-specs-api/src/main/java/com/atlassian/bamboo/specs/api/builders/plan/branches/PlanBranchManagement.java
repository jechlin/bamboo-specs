package com.atlassian.bamboo.specs.api.builders.plan.branches;

import com.atlassian.bamboo.specs.api.builders.EntityPropertiesBuilder;
import com.atlassian.bamboo.specs.api.exceptions.PropertiesValidationException;
import com.atlassian.bamboo.specs.api.model.plan.branches.BranchCleanupProperties;
import com.atlassian.bamboo.specs.api.model.plan.branches.BranchIntegrationProperties;
import com.atlassian.bamboo.specs.api.model.plan.branches.PlanBranchManagementProperties;
import com.atlassian.bamboo.specs.api.model.plan.branches.CreatePlanBranchesProperties;
import org.jetbrains.annotations.NotNull;

import java.time.Duration;

import static com.atlassian.bamboo.specs.api.validators.common.ImporterUtils.checkNotNull;

/**
 * Represents configuration of automatic branch management.
 */
@SuppressWarnings("UnusedReturnValue")
public class PlanBranchManagement extends EntityPropertiesBuilder<PlanBranchManagementProperties> {
    private CreatePlanBranchesProperties createPlanBranches = new CreatePlanBranchesProperties(CreatePlanBranchesProperties.Trigger.MANUAL, null);
    private BranchCleanupProperties deletePlanBranches = new BranchCleanupProperties(false, Duration.ZERO, false, Duration.ZERO);
    private PlanBranchManagementProperties.TriggeringOption triggeringOption = PlanBranchManagementProperties.TriggeringOption.INHERITED;
    private PlanBranchManagementProperties.NotificationStrategy notificationStrategy = PlanBranchManagementProperties.NotificationStrategy.NONE;
    private BranchIntegrationProperties branchIntegration = new BranchIntegrationProperties(false, null, false, false);

    private boolean issueLinkingEnabled = true;

    /**
     * Disable automatic plan branch creation.
     */
    public PlanBranchManagement createManually() {
        this.createPlanBranches = new CreatePlanBranchesProperties(CreatePlanBranchesProperties.Trigger.MANUAL, null);
        return this;
    }

    /**
     * Create new plan branches for new pull requests.
     */
    public PlanBranchManagement createForPullRequest() {
        this.createPlanBranches = new CreatePlanBranchesProperties(CreatePlanBranchesProperties.Trigger.PULL_REQUEST, null);
        return this;
    }

    /**
     * Create new plan branches for new branches in default repository.
     */
    public PlanBranchManagement createForVcsBranch() {
        this.createPlanBranches = new CreatePlanBranchesProperties(CreatePlanBranchesProperties.Trigger.BRANCH, null);
        return this;
    }

    /**
     * Create new plan branches for new branches with name which matches pattern.
     * @param pattern regexp to match branch name
     */
    public PlanBranchManagement createForVcsBranchMatching(String pattern) {
        this.createPlanBranches = new CreatePlanBranchesProperties(CreatePlanBranchesProperties.Trigger.BRANCH, pattern);
        return this;
    }

    /**
     * Sets configuration of automatic removal of branches. The feature is turned off by default.
     *
     * @see BranchCleanup
     */
    public PlanBranchManagement delete(@NotNull BranchCleanup removedBranchCleanup) throws PropertiesValidationException {
        checkNotNull("removedBranchCleanup", removedBranchCleanup);
        this.deletePlanBranches = removedBranchCleanup.build();
        return this;
    }

    /**
     * Create plan branch can only be triggered manually only.
     */
    public PlanBranchManagement triggerBuildsManually() {
        this.triggeringOption = PlanBranchManagementProperties.TriggeringOption.MANUAL;
        return this;
    }

    /**
     * Created plan branch will use the same triggers as master plan.
     */
    public PlanBranchManagement triggerBuildsLikeParentPlan() {
        this.triggeringOption = PlanBranchManagementProperties.TriggeringOption.INHERITED;
        return this;
    }

    /**
     * All committers and people who have favourited the branch will be notified for all build failures and the first successful build.
     */
    public PlanBranchManagement notificationForCommitters() {
        this.notificationStrategy = PlanBranchManagementProperties.NotificationStrategy.NOTIFY_COMMITTERS;
        return this;
    }

    /**
     * Use the same notification rules as configured for the master plan.
     */
    public PlanBranchManagement notificationLikeParentPlan() {
        this.notificationStrategy = PlanBranchManagementProperties.NotificationStrategy.INHERIT;
        return this;
    }

    /**
     * No notifications will be sent for the created branch.
     */
    public PlanBranchManagement notificationDisabled() {
        this.notificationStrategy = PlanBranchManagementProperties.NotificationStrategy.NONE;
        return this;
    }

    /**
     * Sets default merge strategy for new branches. By default merging is turned off.
     *
     * @see BranchIntegration
     */
    public PlanBranchManagement branchIntegration(@NotNull BranchIntegration branchIntegration) throws PropertiesValidationException {
        checkNotNull("branchIntegration", branchIntegration);
        this.branchIntegration = branchIntegration.build();
        return this;
    }

    /**
     * Enables/disables automatic JIRA issue link creation when new branch is created. Enabled by default.
     */
    public PlanBranchManagement issueLinkingEnabled(boolean issueLinkingEnabled) throws PropertiesValidationException {
        this.issueLinkingEnabled = issueLinkingEnabled;
        return this;
    }

    protected PlanBranchManagementProperties build() throws PropertiesValidationException {
        return new PlanBranchManagementProperties(
                createPlanBranches,
                deletePlanBranches,
                triggeringOption,
                notificationStrategy,
                branchIntegration,
                issueLinkingEnabled);
    }
}
