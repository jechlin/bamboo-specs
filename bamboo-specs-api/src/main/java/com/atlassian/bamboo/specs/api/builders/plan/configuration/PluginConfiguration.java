package com.atlassian.bamboo.specs.api.builders.plan.configuration;

import com.atlassian.bamboo.specs.api.builders.EntityPropertiesBuilder;
import com.atlassian.bamboo.specs.api.model.plan.configuration.PluginConfigurationProperties;

public abstract class PluginConfiguration<T extends PluginConfigurationProperties> extends EntityPropertiesBuilder<T> {
    protected abstract T build();
}
