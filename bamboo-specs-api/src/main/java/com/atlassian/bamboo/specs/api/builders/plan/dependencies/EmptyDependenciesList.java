package com.atlassian.bamboo.specs.api.builders.plan.dependencies;

import com.atlassian.bamboo.specs.api.builders.plan.PlanIdentifier;
import com.atlassian.bamboo.specs.api.exceptions.PropertiesValidationException;
import com.atlassian.bamboo.specs.api.model.plan.dependencies.EmptyDependenciesListProperties;
import org.jetbrains.annotations.NotNull;

@Deprecated
public class EmptyDependenciesList extends Dependencies {

    @NotNull
    @Override
    public Dependencies configuration(@NotNull DependenciesConfiguration dependenciesConfiguration) throws PropertiesValidationException {
        throw new RuntimeException("configuration can't be set on EmptyDependenciesList");
    }

    @NotNull
    @Override
    public Dependencies childPlans(@NotNull PlanIdentifier... childPlans) {
        throw new RuntimeException("childPlans can't be set on EmptyDependenciesList");
    }

    @Override
    protected EmptyDependenciesListProperties build() {
        return new EmptyDependenciesListProperties();
    }
}
