/**
 * Generic repository viewers, see also com.atlassian.bamboo.specs.builders.repository.viewer for specific viewers.
 */
package com.atlassian.bamboo.specs.api.builders.repository.viewer;
