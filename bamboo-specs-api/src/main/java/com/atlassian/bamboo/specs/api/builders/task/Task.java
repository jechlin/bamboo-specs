package com.atlassian.bamboo.specs.api.builders.task;

import com.atlassian.bamboo.specs.api.builders.EntityPropertiesBuilder;
import com.atlassian.bamboo.specs.api.model.task.TaskProperties;
import org.jetbrains.annotations.NotNull;

/**
 * Represents a Bamboo task. Task is an unit of work within Job.
 */
public abstract class Task<T extends Task, P extends TaskProperties> extends EntityPropertiesBuilder<P> {
    protected boolean taskEnabled = true;
    protected String description = "";

    protected Task() {
    }

    /**
     * Sets the task description.
     */
    public T description(String description) {
        this.description = description;
        return (T) this;
    }

    /**
     * Enabled/disables that task. Task is enabled by default.
     */
    public T enabled(final boolean taskEnabled) {
        this.taskEnabled = taskEnabled;
        return (T) this;
    }

    @NotNull
    protected abstract P build();
}
