/**
 * Annotations used for conversion of {@link com.atlassian.bamboo.specs.api.model.EntityProperties} model into Java.
 */
package com.atlassian.bamboo.specs.api.codegen.annotations;
