package com.atlassian.bamboo.specs.api.model;

import com.atlassian.bamboo.specs.api.exceptions.PropertiesValidationException;
import com.atlassian.bamboo.specs.api.validators.AtlassianModuleValidator;
import org.jetbrains.annotations.NotNull;

import javax.annotation.concurrent.Immutable;
import java.util.Objects;

import static com.atlassian.bamboo.specs.api.validators.common.ImporterUtils.checkNoErrors;
import static com.atlassian.bamboo.specs.api.validators.common.ImporterUtils.checkNotNull;

@Immutable
public final class AtlassianModuleProperties implements EntityProperties {
    private final String completeModuleKey;

    private AtlassianModuleProperties() {
        completeModuleKey = null;
    }

    public AtlassianModuleProperties(@NotNull final String completeModuleKey) throws PropertiesValidationException {
        this.completeModuleKey = completeModuleKey;

        validate();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        AtlassianModuleProperties that = (AtlassianModuleProperties) o;
        return Objects.equals(getCompleteModuleKey(), that.getCompleteModuleKey());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getCompleteModuleKey());
    }

    @NotNull
    public String getCompleteModuleKey() {
        return completeModuleKey;
    }

    @Override
    public void validate() {
        checkNotNull("completeModuleKey", completeModuleKey);
        checkNoErrors(AtlassianModuleValidator.validate(this));
    }
}
