package com.atlassian.bamboo.specs.api.model.deployment;

import com.atlassian.bamboo.specs.api.codegen.annotations.ConstructFrom;
import com.atlassian.bamboo.specs.api.model.EntityProperties;
import com.atlassian.bamboo.specs.api.model.VariableProperties;
import com.atlassian.bamboo.specs.api.model.notification.NotificationProperties;
import com.atlassian.bamboo.specs.api.model.plan.requirement.RequirementProperties;
import com.atlassian.bamboo.specs.api.model.task.TaskProperties;
import com.atlassian.bamboo.specs.api.model.trigger.TriggerProperties;
import com.atlassian.bamboo.specs.api.validators.VariableValidator;
import com.atlassian.bamboo.specs.api.validators.common.ValidationContext;
import com.atlassian.bamboo.specs.api.validators.common.ValidationUtils;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Objects;

import static com.atlassian.bamboo.specs.api.validators.common.ImporterUtils.checkNoErrors;

@ConstructFrom({"name"})
public class EnvironmentProperties implements EntityProperties {

    private String name;
    private String description;
    private List<TaskProperties> tasks;
    private List<TriggerProperties> triggers;
    private List<VariableProperties> variables;
    private List<RequirementProperties> requirements;
    private List<NotificationProperties> notifications;


    private EnvironmentProperties() {
        tasks = Collections.emptyList();
        triggers = Collections.emptyList();
        variables = Collections.emptyList();
        requirements = Collections.emptyList();
        notifications = Collections.emptyList();
    }

    public EnvironmentProperties(@NotNull String name,
                                 @Nullable String description,
                                 @NotNull List<TaskProperties> tasks,
                                 @NotNull List<TriggerProperties> triggers,
                                 @NotNull List<VariableProperties> variables,
                                 @NotNull List<RequirementProperties> requirements,
                                 @NotNull List<NotificationProperties> notifications) {
        this.name = name;
        this.description = description;
        this.tasks = Collections.unmodifiableList(new ArrayList<>(tasks));
        this.triggers = Collections.unmodifiableList(new ArrayList<>(triggers));
        this.variables = Collections.unmodifiableList(new ArrayList<>(variables));
        this.requirements = Collections.unmodifiableList(new ArrayList<>(requirements));
        this.notifications = Collections.unmodifiableList(new ArrayList<>(notifications));

        validate();
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

    public List<TaskProperties> getTasks() {
        return tasks;
    }

    public List<TriggerProperties> getTriggers() {
        return triggers;
    }

    public List<VariableProperties> getVariables() {
        return variables;
    }

    public List<RequirementProperties> getRequirements() {
        return requirements;
    }

    public List<NotificationProperties> getNotifications() {
        return notifications;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        EnvironmentProperties that = (EnvironmentProperties) o;
        return Objects.equals(getName(), that.getName()) &&
                Objects.equals(getDescription(), that.getDescription()) &&
                Objects.equals(getTasks(), that.getTasks()) &&
                Objects.equals(getTriggers(), that.getTriggers()) &&
                Objects.equals(getVariables(), that.getVariables()) &&
                Objects.equals(getRequirements(), that.getRequirements()) &&
                Objects.equals(getNotifications(), that.getNotifications());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getName(), getDescription(), getTasks(), getTriggers(), getVariables(), getRequirements(), getNotifications());
    }

    @Override
    public void validate() {
        final ValidationContext context = ValidationContext.of("Environment");
        ValidationUtils.validateName(context, name);
        checkNoErrors(VariableValidator.validateUniqueVariableNames(variables));
    }
}
