package com.atlassian.bamboo.specs.api.model.deployment;

import com.atlassian.bamboo.specs.api.codegen.annotations.ConstructFrom;
import com.atlassian.bamboo.specs.api.model.EntityProperties;
import com.atlassian.bamboo.specs.api.util.ReleaseNamingHelper;
import com.atlassian.bamboo.specs.api.util.VariableUtils;
import com.atlassian.bamboo.specs.api.validators.common.ValidationContext;
import com.atlassian.bamboo.specs.api.validators.common.ValidationProblem;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;

import static com.atlassian.bamboo.specs.api.validators.common.ImporterUtils.checkNoErrors;
import static com.atlassian.bamboo.specs.api.validators.common.ImporterUtils.checkRequiredNotBlank;
import static com.atlassian.bamboo.specs.api.validators.common.ImporterUtils.checkThat;

@ConstructFrom({"nextVersionName"})
public class ReleaseNamingProperties implements EntityProperties {
    private String nextVersionName;
    private boolean autoIncrement;
    private boolean applicableToBranches;
    private Set<String> variablesToAutoIncrement;

    protected ReleaseNamingProperties() {
        this.variablesToAutoIncrement = Collections.emptySet();
    }

    public ReleaseNamingProperties(String nextVersionName, boolean autoIncrement, boolean applicableToBranches, Set<String> variablesToAutoIncrement) {
        this.nextVersionName = nextVersionName;
        this.autoIncrement = autoIncrement;
        this.applicableToBranches = applicableToBranches;
        this.variablesToAutoIncrement = Collections.unmodifiableSet(new HashSet<>(variablesToAutoIncrement));

        validate();
    }

    public String getNextVersionName() {
        return nextVersionName;
    }

    public boolean isAutoIncrement() {
        return autoIncrement;
    }

    public boolean isApplicableToBranches() {
        return applicableToBranches;
    }

    public Set<String> getVariablesToAutoIncrement() {
        return variablesToAutoIncrement;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        ReleaseNamingProperties that = (ReleaseNamingProperties) o;
        return isAutoIncrement() == that.isAutoIncrement() &&
                isApplicableToBranches() == that.isApplicableToBranches() &&
                Objects.equals(getNextVersionName(), that.getNextVersionName()) &&
                Objects.equals(getVariablesToAutoIncrement(), that.getVariablesToAutoIncrement());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getNextVersionName(), isAutoIncrement(), isApplicableToBranches(), getVariablesToAutoIncrement());
    }

    @Override
    public void validate() {
        final ValidationContext context = ValidationContext.of("Release naming");
        checkRequiredNotBlank(context.with("nextVersionName"), nextVersionName);
        if (autoIncrement) {
            ReleaseNamingHelper.SplitVersionName splitVersionName = ReleaseNamingHelper.splitVersionName(nextVersionName);
            checkThat(context, splitVersionName.getNumberToIncrement() != null, "Cannot identify any numerical values in version to automatically increment");
        }

        final List<ValidationProblem> errors = new ArrayList<>();

        ValidationContext nextVersionContext = context.with("nextVersionName");
        List<VariableUtils.VariableUsage> variableUsages = VariableUtils.findVariableUsages(nextVersionName);
        variableUsages.stream()
                .map(v -> v.getNamespace() + '.' + v.getName())
                .filter(VariableUtils::isPasswordVariable)
                .forEach(v -> errors.add(new ValidationProblem(nextVersionContext, "Variable %s is password variable and cannot be used in version name", v)));

        ValidationContext variablesToAutoIncrementContext = context.with("variablesToAutoIncrement");
        variablesToAutoIncrement.stream()
                .filter(VariableUtils::isPasswordVariable)
                .forEach(v -> errors.add(new ValidationProblem(variablesToAutoIncrementContext, "Variable %s is password variable and cannot be automatically incremented", v)));

        Set<String> variablesUsedInName = variableUsages.stream().map(VariableUtils.VariableUsage::getName).collect(Collectors.toSet());

        variablesToAutoIncrement.stream()
                .filter(v -> !variablesUsedInName.contains(v))
                .forEach(v -> errors.add(new ValidationProblem(variablesToAutoIncrementContext, "Variable %s is not used in version naming pattern", v)));

        checkNoErrors(errors);
    }
}
