package com.atlassian.bamboo.specs.api.model.notification;

import javax.annotation.concurrent.Immutable;
import java.util.List;

@Immutable
@Deprecated
public class EmptyNotificationsListProperties extends NotificationProperties {

    public EmptyNotificationsListProperties() {
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        return true;
    }

    @Override
    public NotificationTypeProperties getType() {
        throw new RuntimeException("EmptyNotificationsListProperties does not have a type");
    }

    @Override
    public List<NotificationRecipientProperties> getRecipients() {
        throw new RuntimeException("EmptyNotificationsListProperties does not have recipients");
    }

    @Override
    public int hashCode() {
        return 1;
    }

    @Override
    public void validate() {
    }
}
