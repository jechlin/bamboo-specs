package com.atlassian.bamboo.specs.api.model.permission;

import com.atlassian.bamboo.specs.api.builders.permission.PermissionType;
import com.atlassian.bamboo.specs.api.exceptions.PropertiesValidationException;
import com.atlassian.bamboo.specs.api.model.EntityProperties;
import com.atlassian.bamboo.specs.api.validators.common.ImporterUtils;
import com.atlassian.bamboo.specs.api.validators.common.ValidationContext;

import javax.annotation.concurrent.Immutable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

@Immutable
public final class AnonymousUserPermissionsProperties implements EntityProperties {

    private final List<PermissionType> permissionTypes;

    private AnonymousUserPermissionsProperties() {
        this.permissionTypes = null;
    }

    public AnonymousUserPermissionsProperties(final Collection<PermissionType> permissionTypes) throws PropertiesValidationException {
        this.permissionTypes = Collections.unmodifiableList(new ArrayList<>(permissionTypes));
        validate();
    }

    public List<PermissionType> getPermissionTypes() {
        return permissionTypes;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        AnonymousUserPermissionsProperties that = (AnonymousUserPermissionsProperties) o;
        return Objects.equals(permissionTypes, that.permissionTypes);
    }

    @Override
    public int hashCode() {
        return Objects.hash(permissionTypes);
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("AnonymousUserPermissionsProperties{");
        sb.append("permissionTypes=").append(permissionTypes);
        sb.append('}');
        return sb.toString();
    }

    @Override
    public void validate() throws PropertiesValidationException {
        final List<PermissionType> permissionsTypesUnsupportedByAnonymousUser = permissionTypes.stream()
                .filter(permissionType -> permissionType != PermissionType.VIEW)
                .collect(Collectors.toList());
        ImporterUtils.checkArgument(ValidationContext.of("Anonymous Permission"),
                permissionsTypesUnsupportedByAnonymousUser.isEmpty(),
                String.format("Anonymous Role only accepts %s permission. Following permissions are not allowed: %s",
                        PermissionType.VIEW,
                        permissionsTypesUnsupportedByAnonymousUser));
    }
}
