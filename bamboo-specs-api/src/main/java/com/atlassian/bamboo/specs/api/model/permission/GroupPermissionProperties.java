package com.atlassian.bamboo.specs.api.model.permission;

import com.atlassian.bamboo.specs.api.builders.permission.PermissionType;
import com.atlassian.bamboo.specs.api.exceptions.PropertiesValidationException;
import com.atlassian.bamboo.specs.api.model.EntityProperties;
import org.jetbrains.annotations.NotNull;

import javax.annotation.concurrent.Immutable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Objects;

@Immutable
public class GroupPermissionProperties implements EntityProperties {

    private final String group;
    private final List<PermissionType> permissionTypes;

    private GroupPermissionProperties() {
        this.group = null;
        this.permissionTypes = null;
    }

    public GroupPermissionProperties(@NotNull final String group,
                                     @NotNull final Collection<PermissionType> permissionTypes) throws PropertiesValidationException {
        this.group = group;
        this.permissionTypes = Collections.unmodifiableList(new ArrayList<>(permissionTypes));
        validate();
    }

    public String getGroup() {
        return group;
    }

    public List<PermissionType> getPermissionTypes() {
        return permissionTypes;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        GroupPermissionProperties that = (GroupPermissionProperties) o;
        return Objects.equals(group, that.group) &&
                Objects.equals(permissionTypes, that.permissionTypes);
    }

    @Override
    public int hashCode() {
        return Objects.hash(group, permissionTypes);
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("GroupPermissionProperties{");
        sb.append("group='").append(group).append('\'');
        sb.append(", permissionTypes=").append(permissionTypes);
        sb.append('}');
        return sb.toString();
    }

    @Override
    public void validate() throws PropertiesValidationException {
    }
}
