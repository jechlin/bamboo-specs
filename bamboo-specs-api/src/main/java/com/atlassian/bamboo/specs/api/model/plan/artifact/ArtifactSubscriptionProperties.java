package com.atlassian.bamboo.specs.api.model.plan.artifact;

import com.atlassian.bamboo.specs.api.codegen.annotations.Setter;
import com.atlassian.bamboo.specs.api.model.EntityProperties;
import org.apache.commons.lang3.StringUtils;

import javax.annotation.concurrent.Immutable;
import java.util.Objects;

@Immutable
public final class ArtifactSubscriptionProperties implements EntityProperties {
    @Setter("artifact")
    private final String artifactName;
    private final String destination;

    private ArtifactSubscriptionProperties() {
        artifactName = null;
        destination = null;
    }

    public ArtifactSubscriptionProperties(final String artifactName,
                                          final String destination) {
        this.artifactName = artifactName;
        this.destination = StringUtils.defaultString(destination);

        validate();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        ArtifactSubscriptionProperties that = (ArtifactSubscriptionProperties) o;
        return Objects.equals(getArtifactName(), that.getArtifactName()) &&
                Objects.equals(getDestination(), that.getDestination());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getArtifactName(), getDestination());
    }

    public String getArtifactName() {
        return artifactName;
    }

    public String getDestination() {
        return destination;
    }

    @Override
    public void validate() {
    }
}
