package com.atlassian.bamboo.specs.api.model.project;

import com.atlassian.bamboo.specs.api.exceptions.PropertiesValidationException;
import com.atlassian.bamboo.specs.api.model.BambooKeyProperties;
import com.atlassian.bamboo.specs.api.model.BambooOidProperties;
import com.atlassian.bamboo.specs.api.model.EntityProperties;
import com.atlassian.bamboo.specs.api.validators.project.ProjectValidator;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.annotation.concurrent.Immutable;
import java.util.Objects;

import static com.atlassian.bamboo.specs.api.validators.common.ImporterUtils.checkNoErrors;

@Immutable
public final class ProjectProperties implements EntityProperties {
    private BambooOidProperties oid;
    private BambooKeyProperties key;
    private String name;
    private String description;

    private ProjectProperties() {
    }

    public ProjectProperties(final BambooOidProperties oid,
                             final BambooKeyProperties key,
                             final String name,
                             final String description) throws PropertiesValidationException {
        this.oid = oid;
        this.key = key;
        this.name = name;
        this.description = description;

        validate();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        ProjectProperties that = (ProjectProperties) o;
        return Objects.equals(getOid(), that.getOid()) &&
                Objects.equals(getKey(), that.getKey()) &&
                Objects.equals(getName(), that.getName()) &&
                Objects.equals(getDescription(), that.getDescription());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getOid(), getKey(), getName(), getDescription());
    }

    @Nullable
    public BambooOidProperties getOid() {
        return oid;
    }

    @Nullable
    public BambooKeyProperties getKey() {
        return key;
    }

    @NotNull
    public String getName() {
        return name;
    }

    @Nullable
    public String getDescription() {
        return description;
    }

    @Override
    public void validate() {
        checkNoErrors(ProjectValidator.validate(this));
    }
}
