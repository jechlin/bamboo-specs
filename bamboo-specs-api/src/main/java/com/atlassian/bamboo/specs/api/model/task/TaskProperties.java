package com.atlassian.bamboo.specs.api.model.task;


import com.atlassian.bamboo.specs.api.builders.Applicability;
import com.atlassian.bamboo.specs.api.exceptions.PropertiesValidationException;
import com.atlassian.bamboo.specs.api.model.AtlassianModuleProperties;
import com.atlassian.bamboo.specs.api.model.EntityProperties;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.EnumSet;
import java.util.Objects;

public abstract class TaskProperties implements EntityProperties {
    private final String description;
    private final boolean enabled;

    protected TaskProperties() {
        this.description = "";
        this.enabled = true;
    }

    public TaskProperties(final String description,
                          final boolean enabled) throws PropertiesValidationException {
        this.description = description;
        this.enabled = enabled;
    }

    @NotNull
    public abstract AtlassianModuleProperties getAtlassianPlugin();

    @Nullable
    public String getDescription() {
        return description;
    }

    public boolean isEnabled() {
        return enabled;
    }

    public EnumSet<Applicability> applicableTo() {
        return EnumSet.allOf(Applicability.class);
    }

    @Override
    public boolean equals(final Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final TaskProperties that = (TaskProperties) o;
        return isEnabled() == that.isEnabled() &&
                Objects.equals(getAtlassianPlugin(), that.getAtlassianPlugin()) &&
                Objects.equals(getDescription(), that.getDescription());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getAtlassianPlugin(), getDescription(), isEnabled());
    }

    @Override
    public void validate() {
    }

    @Override
    public String toString() {
        return "TaskProperties{" +
                "description='" + description + '\'' +
                ", enabled=" + enabled +
                '}';
    }
}
