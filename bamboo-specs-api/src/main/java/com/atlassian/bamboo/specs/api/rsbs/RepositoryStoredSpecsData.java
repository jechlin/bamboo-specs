package com.atlassian.bamboo.specs.api.rsbs;


import com.atlassian.bamboo.specs.api.validators.common.ImporterUtils;

public class RepositoryStoredSpecsData {
    private long vcsLocationId;

    private String branch;

    private String revision;

    private RepositoryStoredSpecsData() {
    }

    public RepositoryStoredSpecsData(final long rsbsVcsLocationId, final String rsbsBranch, final String rsbsRevision) {
        ImporterUtils.checkNotNull("rsbsBranch", rsbsBranch);
        ImporterUtils.checkNotNull("rsbsRevision", rsbsRevision);
        vcsLocationId = rsbsVcsLocationId;
        branch = rsbsBranch;
        revision = rsbsRevision;
    }

    public long getVcsLocationId() {
        return vcsLocationId;
    }

    public String getBranch() {
        return branch;
    }

    public String getRevision() {
        return revision;
    }
}
