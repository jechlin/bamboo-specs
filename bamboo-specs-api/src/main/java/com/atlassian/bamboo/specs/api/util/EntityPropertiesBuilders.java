package com.atlassian.bamboo.specs.api.util;

import com.atlassian.bamboo.specs.api.builders.EntityPropertiesBuilder;
import com.atlassian.bamboo.specs.api.exceptions.PropertiesValidationException;
import com.atlassian.bamboo.specs.api.model.EntityProperties;
import org.jetbrains.annotations.NotNull;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

public final class EntityPropertiesBuilders {
    private EntityPropertiesBuilders() {
    }

    @NotNull
    public static <T extends EntityProperties, B extends EntityPropertiesBuilder<T>> T build(@NotNull B builder) {
        try {
            final Method buildMethod = builder.getClass().getDeclaredMethod("build");
            buildMethod.setAccessible(true);
            // noinspection unchecked
            return (T) buildMethod.invoke(builder);
        } catch (NoSuchMethodException | IllegalAccessException e) {
            throw new IllegalStateException(getErrorMessage(builder), e);
        } catch (InvocationTargetException e) {
            if (e.getCause() instanceof PropertiesValidationException) {
                throw (PropertiesValidationException) e.getCause();
            } else {
                throw new IllegalStateException(getErrorMessage(builder), e);
            }
        }
    }

    @NotNull
    private static String getErrorMessage(@NotNull Object builder) {
        return "Cannot invoke builder on class " + builder.getClass().getName();
    }
}
