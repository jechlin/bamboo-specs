package com.atlassian.bamboo.specs.api.validators;

import com.atlassian.bamboo.specs.api.model.BambooOidProperties;
import com.atlassian.bamboo.specs.api.validators.common.ValidationProblem;
import org.apache.commons.lang3.StringUtils;
import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;

public final class BambooOidValidator {
    private BambooOidValidator() {
    }

    public static List<ValidationProblem> validate(@NotNull final BambooOidProperties oidProperties) {
        List<ValidationProblem> errors = new ArrayList<>();

        final String oid = oidProperties.getOid();

        if (!StringUtils.containsOnly(oid, "0123456789abcdefghijklmnopqrstuvwxyz")) {
            errors.add(new ValidationProblem("OID must be string containing only numbers and lowercase letters [0-9a-z]."));
        }

        try {
            final Long value = Long.valueOf(oid, Character.MAX_RADIX);
        } catch (NumberFormatException e) {
            errors.add(new ValidationProblem("Invalid OID value."));
        }

        return errors;
    }

}
