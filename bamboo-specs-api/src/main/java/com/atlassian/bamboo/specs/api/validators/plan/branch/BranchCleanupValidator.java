package com.atlassian.bamboo.specs.api.validators.plan.branch;

import com.atlassian.bamboo.specs.api.model.plan.branches.BranchCleanupProperties;
import com.atlassian.bamboo.specs.api.validators.common.ValidationProblem;
import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;

public final class BranchCleanupValidator {
    private BranchCleanupValidator() {
    }

    @NotNull
    public static List<ValidationProblem> validate(@NotNull final BranchCleanupProperties moduleProperties) {
        final ArrayList<ValidationProblem> errors = new ArrayList<>();
        if (moduleProperties.isRemoveDeletedFromRepository() && moduleProperties.getRemoveDeletedFromRepositoryPeriod().isNegative()) {
            errors.add(new ValidationProblem("Branch cleanup period for removed repository branches must be non-negative"));
        }

        if (moduleProperties.isRemoveInactiveInRepository() && moduleProperties.getRemoveInactiveInRepositoryPeriod().getSeconds() <= 0) {
            errors.add(new ValidationProblem("Branch cleanup period for inactive repository branches must be more than 0"));
        }
        return errors;
    }
}
