package com.atlassian.bamboo.specs.api.model;

import com.atlassian.bamboo.specs.api.builders.AtlassianModule;
import com.atlassian.bamboo.specs.api.exceptions.PropertiesValidationException;
import com.atlassian.bamboo.specs.api.util.EntityPropertiesBuilders;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import static org.junit.Assert.assertEquals;

public class AtlassianModulePropertiesTest {
    @Rule
    public final ExpectedException expectedException = ExpectedException.none();

    @Test
    public void creatingModulePropertiesWithNullKeyThrowsException() throws Exception {
        expectedException.expect(PropertiesValidationException.class);
        EntityPropertiesBuilders.build(new AtlassianModule(null));
    }

    @Test
    public void modulePropertiesConstructedSuccessfully() throws PropertiesValidationException {
        final String key = "a:b";

        final AtlassianModuleProperties atlassianModuleProperties = EntityPropertiesBuilders.build(new AtlassianModule(key));

        assertEquals(atlassianModuleProperties.getCompleteModuleKey(), key);
    }

    @Test
    public void creatingModulePropertiesWithoutSeparatorInKeyThrowsException() throws PropertiesValidationException {
        expectedException.expect(PropertiesValidationException.class);
        EntityPropertiesBuilders.build(new AtlassianModule("ABC"));
    }

    @Test
    public void creatingModulePropertiesWithEmptyModulePartInKeyThrowsException() throws PropertiesValidationException {
        expectedException.expect(PropertiesValidationException.class);
        EntityPropertiesBuilders.build(new AtlassianModule("   :A"));
    }

    @Test
    public void creatingModulePropertiesWithEmptyPluginPartInKeyThrowsException() throws PropertiesValidationException {
        expectedException.expect(PropertiesValidationException.class);
        EntityPropertiesBuilders.build(new AtlassianModule("A:   "));
    }
}