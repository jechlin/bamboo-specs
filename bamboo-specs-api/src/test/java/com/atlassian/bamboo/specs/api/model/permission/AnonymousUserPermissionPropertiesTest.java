package com.atlassian.bamboo.specs.api.model.permission;

import com.atlassian.bamboo.specs.api.builders.permission.PermissionType;
import com.atlassian.bamboo.specs.api.exceptions.PropertiesValidationException;
import org.junit.Test;

import java.util.Arrays;
import java.util.Collections;

public class AnonymousUserPermissionPropertiesTest {


    @Test(expected = PropertiesValidationException.class)
    public void validateFailsWithIllegalPermissionTypes() throws Exception {
        AnonymousUserPermissionsProperties anonymousUserPermissionProperties = new AnonymousUserPermissionsProperties(
                Arrays.asList(PermissionType.ADMIN, PermissionType.BUILD, PermissionType.VIEW)
        );
        anonymousUserPermissionProperties.validate();
    }


    @Test
    public void validateWithViewOnly() throws Exception {
        AnonymousUserPermissionsProperties anonymousUserPermissionProperties = new AnonymousUserPermissionsProperties(
                Arrays.asList(PermissionType.VIEW)
        );
        anonymousUserPermissionProperties.validate();
    }

    @Test
    public void validateNoPermission() throws Exception {
        AnonymousUserPermissionsProperties anonymousUserPermissionProperties = new AnonymousUserPermissionsProperties(Collections.emptyList());
        anonymousUserPermissionProperties.validate();
    }

}