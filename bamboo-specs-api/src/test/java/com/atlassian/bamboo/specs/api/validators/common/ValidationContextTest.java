package com.atlassian.bamboo.specs.api.validators.common;

import org.junit.Test;

import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.isEmptyString;
import static org.junit.Assert.assertThat;

public class ValidationContextTest {
    @Test
    public void testContextPath() {
        // one element
        assertThat(ValidationContext.of("").toString(), isEmptyString());
        assertThat(ValidationContext.of("ABC").toString(), equalTo("ABC"));

        // many elements
        assertThat(ValidationContext.of("ABC").with("DEF").with("GHI").toString(), equalTo("ABC / DEF / GHI"));
    }

}