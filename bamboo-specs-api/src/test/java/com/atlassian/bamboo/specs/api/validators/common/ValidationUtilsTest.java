package com.atlassian.bamboo.specs.api.validators.common;

import org.apache.commons.lang3.StringUtils;
import org.junit.Test;

import java.lang.reflect.Method;
import java.net.URI;
import java.util.Arrays;
import java.util.Collection;
import java.util.Set;
import java.util.TreeSet;
import java.util.stream.Collectors;

import static com.atlassian.bamboo.specs.api.validators.common.ValidationUtils.containsBambooVariable;
import static com.atlassian.bamboo.specs.api.validators.common.ValidationUtils.repositoryUriContainsPassword;
import static com.atlassian.bamboo.specs.api.validators.common.ValidationUtils.validateBoolean;
import static com.atlassian.bamboo.specs.api.validators.common.ValidationUtils.validateDescription;
import static com.atlassian.bamboo.specs.api.validators.common.ValidationUtils.validateEnum;
import static com.atlassian.bamboo.specs.api.validators.common.ValidationUtils.validateLong;
import static com.atlassian.bamboo.specs.api.validators.common.ValidationUtils.validateName;
import static com.atlassian.bamboo.specs.api.validators.common.ValidationUtils.validateNotContainsRelaxedXssRelatedCharacters;
import static com.atlassian.bamboo.specs.api.validators.common.ValidationUtils.validateNotContainsShellInjectionRelatedCharacters;
import static com.atlassian.bamboo.specs.api.validators.common.ValidationUtils.validateNotContainsXssRelatedCharacters;
import static com.atlassian.bamboo.specs.api.validators.common.ValidationUtils.validatePositive;
import static com.atlassian.bamboo.specs.api.validators.common.ValidationUtils.validateRequired;
import static com.atlassian.bamboo.specs.api.validators.common.ValidationUtils.validateRequiredBoolean;
import static com.atlassian.bamboo.specs.api.validators.common.ValidationUtils.validateRequiredEnum;
import static com.atlassian.bamboo.specs.api.validators.common.ValidationUtils.validateRequiredLong;
import static com.atlassian.bamboo.specs.api.validators.common.ValidationUtils.validateUrl;
import static org.hamcrest.Matchers.contains;
import static org.hamcrest.Matchers.containsInAnyOrder;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.not;
import static org.hamcrest.Matchers.startsWith;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;

public class ValidationUtilsTest {

    private final ValidationContext validationContext = ValidationContext.of("context");

    private enum MyEnum {
        ONE, TWO, THREE
    }

    /**
     * To avoid confusion, methods from {@link ValidationUtils} should not start with {@code #check} prefix, since such
     * methods might be expected to throw exception if the condition is not met.
     * <p>
     * Throwing methods prefixed with #check should be used in {@link ImporterUtils} instead (e.g.
     * {@link ImporterUtils#checkRequired(ValidationContext, Object)}).
     */
    @Test
    public void testMethodsAreNotPrefixedWithCheck() {
        final Method[] declaredMethods = ValidationUtils.class.getDeclaredMethods();
        for (Method declaredMethod : declaredMethods) {
            assertThat(declaredMethod.getName(), not(startsWith("check")));
        }
    }

    @Test
    public void testContainsBambooVariable() throws Exception {
        assertFalse(containsBambooVariable(null));
        assertFalse(containsBambooVariable(""));
        assertFalse(containsBambooVariable("${other"));
        assertTrue(containsBambooVariable("prefix ${bamboo} suffix"));
        assertTrue(containsBambooVariable("prefix ${bamboo.abc} suffix"));
    }

    @Test
    public void testRepositoryUriContainsPassword() throws Exception {
        assertFalse(repositoryUriContainsPassword(new URI("http", null, "host", 80, null, null, null)));
        assertFalse(repositoryUriContainsPassword(new URI("http", "user", "host", 80, null, null, null)));
        assertTrue(repositoryUriContainsPassword(new URI("http", "user:password", "host", 80, null, null, null)));
    }

    @Test
    public void testValidateRequired() throws Exception {
        assertThat(validateRequired(validationContext, null).get().getMessage(),
                equalTo("context: must be defined"));

        assertFalse(validateRequired(validationContext, "abc").isPresent());

    }

    @Test
    public void testValidateRequiredString() throws Exception {
        assertThat(ValidationUtils.validateRequiredString(validationContext, null).get().getMessage(),
                equalTo("context: must be defined"));

        assertThat(ValidationUtils.validateRequiredString(validationContext, "").get().getMessage(),
                equalTo("context: can not be empty"));
    }

    @Test
    public void testValidateBoolean() throws Exception {
        assertFalse(validateBoolean(validationContext, null).isPresent());

        assertThat(validateBoolean(validationContext, "abc").get().getMessage(),
                equalTo("context: must be boolean value but is 'abc'"));

        assertFalse(validateBoolean(validationContext, "true").isPresent());
    }

    @Test
    public void testValidateRequiredBoolean() throws Exception {
        assertThat(validateRequiredBoolean(validationContext, null).get().getMessage(),
                equalTo("context: must be defined"));

        assertThat(validateRequiredBoolean(validationContext, "abc").get().getMessage(),
                equalTo("context: must be boolean value but is 'abc'"));

        assertFalse(validateRequiredBoolean(validationContext, "true").isPresent());
    }

    @Test
    public void testValidateLong() throws Exception {
        assertFalse(validateLong(validationContext, null).isPresent());

        assertThat(validateLong(validationContext, "abc").get().getMessage(),
                equalTo("context: must be numerical long value but is 'abc'"));

        assertFalse(validateLong(validationContext, "1234567890123456789").isPresent());
    }

    @Test
    public void testValidateRequiredLong() throws Exception {
        assertThat(validateRequiredLong(validationContext, null).get().getMessage(),
                equalTo("context: must be defined"));

        assertThat(validateRequiredLong(validationContext, "abc").get().getMessage(),
                equalTo("context: must be numerical long value but is 'abc'"));

        assertFalse(validateRequiredLong(validationContext, "1234567890123456789").isPresent());
    }

    @Test
    public void testValidateEnum() throws Exception {
        assertFalse(validateEnum(validationContext, null, MyEnum.class).isPresent());

        assertThat(validateEnum(validationContext, "abc", MyEnum.class).get().getMessage(),
                equalTo("context: must be enum value of {ONE,TWO,THREE} but is 'abc'"));

        assertFalse(validateEnum(validationContext, "ONE", MyEnum.class).isPresent());
    }

    @Test
    public void testValidateRequiredEnum() throws Exception {
        assertThat(validateRequiredEnum(validationContext, null, MyEnum.class).get().getMessage(),
                equalTo("context: must be defined"));

        assertThat(validateRequiredEnum(validationContext, "abc", MyEnum.class).get().getMessage(),
                equalTo("context: must be enum value of {ONE,TWO,THREE} but is 'abc'"));

        assertFalse(validateRequiredEnum(validationContext, "ONE", MyEnum.class).isPresent());
    }

    @Test
    public void testValidatePositive() throws Exception {
        assertFalse(validatePositive(validationContext, 0).isPresent());
        assertFalse(validatePositive(validationContext, 123).isPresent());
        assertFalse(ValidationUtils.validatePositive(validationContext, Long.MAX_VALUE).isPresent());

        assertThat(validatePositive(validationContext, -1).get().getMessage(),
                equalTo("context: must be >= 0 but is -1"));
        assertThat(ValidationUtils.validatePositive(validationContext, Long.MIN_VALUE).get().getMessage(),
                equalTo("context: must be >= 0 but is -9223372036854775808"));
    }

    @Test
    public void testValidateUrl() throws Exception {
        final Set<String> schemes = new TreeSet<>(Arrays.asList("ftp", "http"));
        assertFalse(validateUrl(validationContext, "http://localhost", schemes).isPresent());
        assertThat(validateUrl(validationContext, "ssh://localhost", schemes).get().getMessage(),
                equalTo("context: scheme 'ssh' is not supported - supported schemes are: ftp, http"));
    }

    @Test
    public void testValidateNotContainsXssRelatedCharacters() throws Exception {
        assertFalse(validateNotContainsXssRelatedCharacters(validationContext, "abc").isPresent());
        assertThat(validateNotContainsXssRelatedCharacters(validationContext, "ab\\c").get().getMessage(),
                equalTo("context: can not contain any of those characters: [\", &, ', <, >, \\] but it is 'ab\\c'"));
    }

    @Test
    public void testValidateNotContainsRelaxedXssRelatedCharacters() throws Exception {
        assertFalse(validateNotContainsRelaxedXssRelatedCharacters(validationContext, "abc").isPresent());
        assertFalse(validateNotContainsRelaxedXssRelatedCharacters(validationContext, "ab\\c").isPresent());
        assertThat(validateNotContainsRelaxedXssRelatedCharacters(validationContext, "ab&c").get().getMessage(),
                equalTo("context: can not contain any of those characters: [\", &, ', <, >] but it is 'ab&c'"));
    }

    @Test
    public void testValidateNotContainsShellInjectionRelatedCharacters() throws Exception {
        assertFalse(validateNotContainsShellInjectionRelatedCharacters(validationContext, "abc").isPresent());
        assertThat(validateNotContainsShellInjectionRelatedCharacters(validationContext, "ab\"c").get().getMessage(),
                equalTo("context: can not contain any of those characters: [\", &, ', >, <, ;, `], nor this substring: '$(' but it is 'ab\"c'"));
        assertThat(validateNotContainsShellInjectionRelatedCharacters(validationContext, "ab$(c)").get().getMessage(),
                equalTo("context: can not contain any of those characters: [\", &, ', >, <, ;, `], nor this substring: '$(' but it is 'ab$(c)'"));
    }

    @Test
    public void testValidateName() throws Exception {
        final Collection<String> nullNameMessages = validateName(validationContext, null)
                .stream()
                .map(ValidationProblem::getMessage)
                .collect(Collectors.toList());
        assertThat(nullNameMessages, contains("context / Name: must be defined"));

        final Collection<String> emptyNameMessages = validateName(validationContext, "")
                .stream()
                .map(ValidationProblem::getMessage)
                .collect(Collectors.toList());
        assertThat(emptyNameMessages, contains("context / Name: can not be empty"));

        final String longNameWithXss = StringUtils.repeat("ABC&DEF&", 50);
        final Collection<String> xssAndTooLongNameMessages = validateName(validationContext, longNameWithXss)
                .stream()
                .map(ValidationProblem::getMessage)
                .collect(Collectors.toList());
        assertThat(xssAndTooLongNameMessages,
                containsInAnyOrder(
                        "context / Name: it can not be longer than 255 characters but has 400",
                        "context / Name: can not contain any of those characters: [\", &, ', <, >, \\] but it is '" + longNameWithXss + "'"));
    }

    @Test
    public void testValidateDescription() throws Exception {
        final Collection<String> xssDescriptonMessages = validateDescription(validationContext, "a&b")
                .stream()
                .map(ValidationProblem::getMessage)
                .collect(Collectors.toList());
        assertThat(xssDescriptonMessages,
                contains("context / Description: can not contain any of those characters: [\", &, ', <, >, \\] but it is 'a&b'"));
    }

}