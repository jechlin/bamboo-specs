package com.atlassian.bamboo.specs.builders.notification;

import com.atlassian.bamboo.specs.api.builders.notification.NotificationRecipient;
import com.atlassian.bamboo.specs.api.validators.common.ImporterUtils;
import com.atlassian.bamboo.specs.model.notification.EmailRecipientProperties;
import org.jetbrains.annotations.NotNull;

/**
 * Represents email address Bamboo can send notifications to.
 */
public class EmailRecipient extends NotificationRecipient<EmailRecipient, EmailRecipientProperties> {
    private String email;

    public EmailRecipient(String email) {
        ImporterUtils.checkNotBlank("email", email);
        this.email = email;
    }

    @NotNull
    @Override
    protected EmailRecipientProperties build() {
        return new EmailRecipientProperties(email);
    }
}
