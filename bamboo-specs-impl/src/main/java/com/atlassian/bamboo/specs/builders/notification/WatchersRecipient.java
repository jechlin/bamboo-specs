package com.atlassian.bamboo.specs.builders.notification;

import com.atlassian.bamboo.specs.api.builders.Applicability;
import com.atlassian.bamboo.specs.api.builders.notification.NotificationRecipient;
import com.atlassian.bamboo.specs.api.model.AtlassianModuleProperties;
import com.atlassian.bamboo.specs.api.model.notification.AnyNotificationRecipientProperties;
import org.jetbrains.annotations.NotNull;

import java.util.EnumSet;

/**
 * Represents a users who have marked a plan as their favourite.
 */
public class WatchersRecipient extends NotificationRecipient<WatchersRecipient, AnyNotificationRecipientProperties> {

    private static final AtlassianModuleProperties ATLASSIAN_PLUGIN =
            new AtlassianModuleProperties("com.atlassian.bamboo.plugin.system.notifications:recipient.watcher");

    @NotNull
    @Override
    protected AnyNotificationRecipientProperties build() {
        return new AnyNotificationRecipientProperties(ATLASSIAN_PLUGIN, "", EnumSet.of(Applicability.PLANS));
    }
}
