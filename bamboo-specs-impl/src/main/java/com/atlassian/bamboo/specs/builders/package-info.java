/**
 * <b>The 'builders.*' packages contain classes you can use to define your Bamboo configuration as code.</b>
 */
package com.atlassian.bamboo.specs.builders;
