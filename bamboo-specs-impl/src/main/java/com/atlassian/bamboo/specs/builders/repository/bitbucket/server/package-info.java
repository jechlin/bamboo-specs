/**
 * Bitbucket Server repository type.
 */
package com.atlassian.bamboo.specs.builders.repository.bitbucket.server;
