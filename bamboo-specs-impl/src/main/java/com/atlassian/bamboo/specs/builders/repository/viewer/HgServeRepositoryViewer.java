package com.atlassian.bamboo.specs.builders.repository.viewer;

import com.atlassian.bamboo.specs.api.builders.repository.viewer.VcsRepositoryViewer;
import com.atlassian.bamboo.specs.api.exceptions.PropertiesValidationException;
import com.atlassian.bamboo.specs.api.model.AtlassianModuleProperties;
import com.atlassian.bamboo.specs.api.model.repository.viewer.AnyVcsRepositoryViewerProperties;
import com.atlassian.bamboo.specs.api.model.repository.viewer.VcsRepositoryViewerProperties;
import org.jetbrains.annotations.NotNull;

import java.util.Collections;

/**
 * Represents viewer that generates links to default hg repository browser service (a.k.a hg serve).
 */
public class HgServeRepositoryViewer extends VcsRepositoryViewer {
    private static final AtlassianModuleProperties ATLASSIAN_PLUGIN =
            new AtlassianModuleProperties("com.atlassian.bamboo.plugins.atlassian-bamboo-plugin-mercurial:hgServeViewer");


    @NotNull
    protected VcsRepositoryViewerProperties build() throws PropertiesValidationException {
        return new AnyVcsRepositoryViewerProperties(ATLASSIAN_PLUGIN, Collections.emptyMap());
    }
}
