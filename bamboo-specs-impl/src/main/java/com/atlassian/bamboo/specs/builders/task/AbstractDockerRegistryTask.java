package com.atlassian.bamboo.specs.builders.task;

import com.atlassian.bamboo.specs.model.task.docker.DockerRegistryTaskProperties;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;


/**
 * Class containing common parts to Docker tasks which operates on Docker registry, like {@link DockerPushImageTask} or
 * {@link DockerPullImageTask}.
 */
public abstract class AbstractDockerRegistryTask<T extends AbstractDockerRegistryTask> extends AbstractDockerTask<T, DockerRegistryTaskProperties> {
    @NotNull
    protected String image;
    @NotNull
    protected DockerRegistryTaskProperties.RegistryType registryType;
    @Nullable
    protected String username;
    @Nullable
    protected String password;
    @Nullable
    protected String email;

    // Javadocs shall be defined in subclasses.
    public T dockerHubImage(@NotNull String image) {
        this.image = image;
        this.registryType = DockerRegistryTaskProperties.RegistryType.DOCKER_HUB;
        return (T) this;
    }

    // Javadocs shall be defined in subclasses.
    public T customRegistryImage(@NotNull String image) {
        this.image = image;
        this.registryType = DockerRegistryTaskProperties.RegistryType.CUSTOM;
        return (T) this;
    }

    /**
     * Sets authentication settings to authenticate to Docker registry. All fields are required.
     * You can also use default authentication method by {@link #defaultAuthentication()}
     */
    public T authentication(@NotNull String username, @NotNull String password, @NotNull String email) {
        this.username = username;
        this.password = password;
        this.email = email;
        return (T) this;
    }

    /**
     * Use agent's <i>~/.dockercfg</i> credentials to authenticate to Docker registry.
     */
    public T defaultAuthentication() {
        username = null;
        password = null;
        email = null;
        return (T) this;
    }
}
