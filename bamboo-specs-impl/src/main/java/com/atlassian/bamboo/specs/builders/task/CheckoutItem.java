package com.atlassian.bamboo.specs.builders.task;

import com.atlassian.bamboo.specs.api.builders.EntityPropertiesBuilder;
import com.atlassian.bamboo.specs.api.builders.repository.VcsRepositoryIdentifier;
import com.atlassian.bamboo.specs.api.model.repository.VcsRepositoryIdentifierProperties;
import com.atlassian.bamboo.specs.api.util.EntityPropertiesBuilders;
import com.atlassian.bamboo.specs.model.task.CheckoutItemProperties;
import org.jetbrains.annotations.NotNull;

import static com.atlassian.bamboo.specs.api.validators.common.ImporterUtils.checkNotNull;

/**
 * Represents a single checkout request.
 */
public class CheckoutItem extends EntityPropertiesBuilder<CheckoutItemProperties> {
    private VcsRepositoryIdentifierProperties repository;
    private boolean defaultRepository;
    private String path = "";

    public CheckoutItem() {
    }

    /**
     * Sets this checkout request for plan's default repository.
     * Default repository is the repository which is the first on the list of plan's repositories.
     */
    public CheckoutItem defaultRepository() {
        this.defaultRepository = true;
        return this;
    }

    /**
     * Sets this checkout request for a particular repository.
     */
    public CheckoutItem repository(@NotNull String repositoryName) {
        checkNotNull("repositoryName", repositoryName);
        return repository(new VcsRepositoryIdentifier().name(repositoryName));
    }

    /**
     * Sets this checkout request for a particular repository.
     */
    public CheckoutItem repository(@NotNull VcsRepositoryIdentifier repositoryIdentifier) {
        checkNotNull("repositoryIdentifier", repositoryIdentifier);
        this.repository = EntityPropertiesBuilders.build(repositoryIdentifier);
        return this;
    }

    /**
     * Sets the path the repository should be checked out to. The path must be relative to the working directory.
     * Empty by default.
     */
    public CheckoutItem path(@NotNull String path) {
        checkNotNull("path", path);
        this.path = path;
        return this;
    }

    @Override
    protected CheckoutItemProperties build() {
        return new CheckoutItemProperties(repository, path, defaultRepository);
    }
}
