package com.atlassian.bamboo.specs.builders.task;

import com.atlassian.bamboo.specs.api.builders.task.Task;
import com.atlassian.bamboo.specs.model.task.CleanWorkingDirectoryTaskProperties;

/**
 * Represents a task that cleans build's working directory.
 */
public class CleanWorkingDirectoryTask extends Task<CleanWorkingDirectoryTask, CleanWorkingDirectoryTaskProperties> {
    @Override
    protected CleanWorkingDirectoryTaskProperties build() {
        return new CleanWorkingDirectoryTaskProperties(description, taskEnabled);
    }
}
