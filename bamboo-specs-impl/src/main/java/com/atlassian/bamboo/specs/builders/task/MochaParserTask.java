package com.atlassian.bamboo.specs.builders.task;

import com.atlassian.bamboo.specs.api.builders.task.Task;
import com.atlassian.bamboo.specs.api.validators.common.ImporterUtils;
import com.atlassian.bamboo.specs.model.task.MochaParserTaskProperties;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import static com.atlassian.bamboo.specs.api.util.InliningUtils.preventInlining;

/**
 * Parse test results of Mocha executed with 'mocha-bamboo-reporter'.
 * <p>
 * For convenience, it's advised to use {@link MochaRunnerTask} which is capable of both executing tests and parsing
 * test results. {@link MochaParserTask} should only be used if the tests were executed in a different manner.
 *
 * @see <a href="https://mochajs.org/">mochajs.org</a>
 */
public class MochaParserTask extends Task<MochaParserTask, MochaParserTaskProperties> {
    public static final String DEFAULT_TEST_FILE_PATTERN = preventInlining("mocha.json");

    @NotNull
    private String testFilePattern = DEFAULT_TEST_FILE_PATTERN;
    @Nullable
    private String workingSubdirectory;
    private boolean pickUpTestResultsCreatedOutsideOfThisBuild;

    /**
     * Ant-style pattern of test results file to parse. If not set, the default value of 'mocha.json' will be used.
     */
    public MochaParserTask testFilePattern(@NotNull String testFilePattern) {
        ImporterUtils.checkNotNull("testFilePattern", testFilePattern);
        this.testFilePattern = testFilePattern;
        return this;
    }

    /**
     * Specify an alternative subdirectory as working directory for the task.
     */
    public MochaParserTask workingSubdirectory(@Nullable String workingSubdirectory) {
        this.workingSubdirectory = workingSubdirectory;
        return this;
    }

    /**
     * When set to true, files created before the current build was started will be analyzed as valid tests results.
     */
    public MochaParserTask pickUpTestResultsCreatedOutsideOfThisBuild(boolean pickUpTestResultsCreatedOutsideOfThisBuild) {
        this.pickUpTestResultsCreatedOutsideOfThisBuild = pickUpTestResultsCreatedOutsideOfThisBuild;
        return this;
    }

    @NotNull
    @Override
    protected MochaParserTaskProperties build() {
        return new MochaParserTaskProperties(
                description,
                taskEnabled,
                testFilePattern,
                workingSubdirectory,
                pickUpTestResultsCreatedOutsideOfThisBuild);
    }
}
