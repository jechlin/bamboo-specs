/**
 * Tasks you can execute in a job, such as: source code checkout, Maven build or a shell script.
 */
package com.atlassian.bamboo.specs.builders.task;
