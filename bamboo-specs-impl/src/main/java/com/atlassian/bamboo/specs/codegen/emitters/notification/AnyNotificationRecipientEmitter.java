package com.atlassian.bamboo.specs.codegen.emitters.notification;

import com.atlassian.bamboo.specs.api.builders.notification.AnyNotificationRecipient;
import com.atlassian.bamboo.specs.api.builders.notification.NotificationRecipient;
import com.atlassian.bamboo.specs.api.codegen.CodeGenerationContext;
import com.atlassian.bamboo.specs.api.codegen.CodeGenerationException;
import com.atlassian.bamboo.specs.api.model.notification.AnyNotificationRecipientProperties;
import com.atlassian.bamboo.specs.builders.notification.CommittersRecipient;
import com.atlassian.bamboo.specs.builders.notification.ResponsibleRecipient;
import com.atlassian.bamboo.specs.builders.notification.WatchersRecipient;
import com.atlassian.bamboo.specs.codegen.emitters.value.EntityPropertiesEmitter;
import com.atlassian.bamboo.specs.util.MapBuilder;
import org.jetbrains.annotations.NotNull;

import java.util.Map;

public class AnyNotificationRecipientEmitter extends EntityPropertiesEmitter<AnyNotificationRecipientProperties> {

    private static final Map<String, Class<? extends NotificationRecipient>> VIEWER_BUILDERS = new MapBuilder<String, Class<? extends NotificationRecipient>>()
            .put("com.atlassian.bamboo.plugin.system.notifications:recipient.watcher", WatchersRecipient.class)
            .put("com.atlassian.bamboo.plugin.system.notifications:recipient.committer", CommittersRecipient.class)
            .put("com.atlassian.bamboo.plugin.system.notifications:recipient.responsible", ResponsibleRecipient.class)
            .build();

    @NotNull
    @Override
    public String emitCode(@NotNull final CodeGenerationContext context, @NotNull final AnyNotificationRecipientProperties entity) throws CodeGenerationException {
        builderClass = AnyNotificationRecipient.class;
        if (VIEWER_BUILDERS.containsKey(entity.getAtlassianPlugin().getCompleteModuleKey())) {
            builderClass = VIEWER_BUILDERS.get(entity.getAtlassianPlugin().getCompleteModuleKey());
            return "new " + context.importClassName(builderClass) + "()";
        }
        return super.emitCode(context, entity);
    }
}
