package com.atlassian.bamboo.specs.codegen.emitters.notification;

import com.atlassian.bamboo.specs.api.builders.notification.AnyNotificationType;
import com.atlassian.bamboo.specs.api.builders.notification.NotificationType;
import com.atlassian.bamboo.specs.api.codegen.CodeGenerationContext;
import com.atlassian.bamboo.specs.api.codegen.CodeGenerationException;
import com.atlassian.bamboo.specs.api.model.notification.AnyNotificationTypeProperties;
import com.atlassian.bamboo.specs.builders.notification.CommentAddedNotification;
import com.atlassian.bamboo.specs.builders.notification.DeploymentFailedNotification;
import com.atlassian.bamboo.specs.builders.notification.DeploymentFinishedNotification;
import com.atlassian.bamboo.specs.builders.notification.JobFailedNotification;
import com.atlassian.bamboo.specs.builders.notification.FirstFailedJobNotification;
import com.atlassian.bamboo.specs.builders.notification.JobCompletedNotification;
import com.atlassian.bamboo.specs.builders.notification.JobHungNotification;
import com.atlassian.bamboo.specs.builders.notification.JobTimeoutNotification;
import com.atlassian.bamboo.specs.builders.notification.JobStatusChangedNotification;
import com.atlassian.bamboo.specs.builders.notification.JobWithoutAgentNotification;
import com.atlassian.bamboo.specs.builders.notification.PlanCompletedNotification;
import com.atlassian.bamboo.specs.builders.notification.PlanFailedNotification;
import com.atlassian.bamboo.specs.builders.notification.PlanStatusChangedNotification;
import com.atlassian.bamboo.specs.builders.notification.ResponsibilityChangedNotification;
import com.atlassian.bamboo.specs.codegen.emitters.value.EntityPropertiesEmitter;
import com.atlassian.bamboo.specs.util.MapBuilder;
import org.jetbrains.annotations.NotNull;

import java.util.Map;

public class AnyNotificationTypeEmitter extends EntityPropertiesEmitter<AnyNotificationTypeProperties> {

    private static final Map<String, Class<? extends NotificationType>> VIEWER_BUILDERS = new MapBuilder<String, Class<? extends NotificationType>>()
            .put("bamboo.deployments:deploymentFailed", DeploymentFailedNotification.class)
            .put("bamboo.deployments:deploymentFinished", DeploymentFinishedNotification.class)
            .put("com.atlassian.bamboo.plugin.system.notifications:buildCommented", CommentAddedNotification.class)
            .put("com.atlassian.bamboo.plugin.system.notifications:buildCompleted.failedBuilds", JobFailedNotification.class)
            .put("com.atlassian.bamboo.plugin.system.notifications:buildCompleted.firstJobFailed", FirstFailedJobNotification.class)
            .put("com.atlassian.bamboo.plugin.system.notifications:buildCompleted.allBuilds", JobCompletedNotification.class)
            .put("com.atlassian.bamboo.plugin.system.notifications:buildHung", JobHungNotification.class)
            .put("com.atlassian.bamboo.plugin.system.notifications:buildQueueTimeout", JobTimeoutNotification.class)
            .put("com.atlassian.bamboo.plugin.system.notifications:buildCompleted.changedBuildStatus", JobStatusChangedNotification.class)
            .put("com.atlassian.bamboo.plugin.system.notifications:buildMissingCapableAgent", JobWithoutAgentNotification.class)
            .put("com.atlassian.bamboo.plugin.system.notifications:chainCompleted.allBuilds", PlanCompletedNotification.class)
            .put("com.atlassian.bamboo.plugin.system.notifications:chainCompleted.failedChains", PlanFailedNotification.class)
            .put("com.atlassian.bamboo.plugin.system.notifications:chainCompleted.changedChainStatus", PlanStatusChangedNotification.class)
            .put("com.atlassian.bamboo.brokenbuildtracker:responsibilityChanged", ResponsibilityChangedNotification.class)
            .build();

    @NotNull
    @Override
    public String emitCode(@NotNull final CodeGenerationContext context, @NotNull final AnyNotificationTypeProperties entity) throws CodeGenerationException {
        builderClass = AnyNotificationType.class;
        if (VIEWER_BUILDERS.containsKey(entity.getAtlassianPlugin().getCompleteModuleKey())) {
            builderClass = VIEWER_BUILDERS.get(entity.getAtlassianPlugin().getCompleteModuleKey());
            return "new " + context.importClassName(builderClass) + "()";
        }
        return super.emitCode(context, entity);
    }
}
