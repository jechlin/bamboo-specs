package com.atlassian.bamboo.specs.codegen.emitters.repository;

public class GitAuthenticationEmitter extends AuthenticationEmitter {
    public GitAuthenticationEmitter() {
        super("authentication");
    }
}
