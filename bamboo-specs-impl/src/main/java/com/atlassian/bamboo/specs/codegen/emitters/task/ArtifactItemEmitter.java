package com.atlassian.bamboo.specs.codegen.emitters.task;

import com.atlassian.bamboo.specs.api.codegen.CodeGenerationContext;
import com.atlassian.bamboo.specs.api.codegen.CodeGenerationException;
import com.atlassian.bamboo.specs.builders.task.ArtifactItem;
import com.atlassian.bamboo.specs.codegen.emitters.value.EntityPropertiesEmitter;
import com.atlassian.bamboo.specs.model.task.ArtifactItemProperties;
import org.jetbrains.annotations.NotNull;

public class ArtifactItemEmitter extends EntityPropertiesEmitter<ArtifactItemProperties> {
    @NotNull
    @Override
    public String emitCode(@NotNull CodeGenerationContext context, @NotNull ArtifactItemProperties value) throws CodeGenerationException {
        builderClass = ArtifactItem.class;
        fieldsToSkip.add("allArtifacts");
        fieldsToSkip.add("artifactName");

        final String constructorCode = emitConstructorInvocation(context, value);
        final String otherFieldsCode = emitFields(context, value);

        try {
            context.incIndentation();
            return String.format("%s%s%s%s",
                    constructorCode,
                    otherFieldsCode,
                    context.newLine(),
                    value.isAllArtifacts()
                            ? ".allArtifacts()"
                            : ".artifact(\"" + value.getArtifactName() + "\")");
        } finally {
            context.decIndentation();
        }
    }
}
