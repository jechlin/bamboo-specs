package com.atlassian.bamboo.specs.codegen.emitters.task;

import com.atlassian.bamboo.specs.api.codegen.CodeEmitter;
import com.atlassian.bamboo.specs.api.codegen.CodeGenerationContext;
import com.atlassian.bamboo.specs.api.codegen.CodeGenerationException;
import org.jetbrains.annotations.NotNull;

public class MavenVersionEmitter implements CodeEmitter<Integer> {
    @NotNull
    @Override
    public String emitCode(@NotNull final CodeGenerationContext context, @NotNull final Integer value) throws CodeGenerationException {
        return ".version" + value + "()";
    }
}
