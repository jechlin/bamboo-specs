package com.atlassian.bamboo.specs.codegen.emitters.task;

import com.atlassian.bamboo.specs.api.codegen.CodeEmitter;
import com.atlassian.bamboo.specs.api.codegen.CodeGenerationContext;
import com.atlassian.bamboo.specs.api.codegen.CodeGenerationException;
import com.atlassian.bamboo.specs.builders.task.NUnitRunnerTask;
import org.jetbrains.annotations.NotNull;

public class NUnitVersionEmitter implements CodeEmitter<NUnitRunnerTask.NUnitVersion> {
    @NotNull
    @Override
    public String emitCode(@NotNull final CodeGenerationContext context, @NotNull final NUnitRunnerTask.NUnitVersion version) throws CodeGenerationException {
        switch (version) {
            case NUNIT_2:
                return ".nUnitVersion2()";
            case NUNIT_3:
                return ".nUnitVersion3()";
            default:
                throw new IllegalStateException("unknown version: " + version);
        }
    }
}
