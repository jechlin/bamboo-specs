package com.atlassian.bamboo.specs.codegen.emitters.task;

import com.atlassian.bamboo.specs.api.codegen.CodeGenerationContext;
import com.atlassian.bamboo.specs.api.codegen.CodeGenerationException;
import com.atlassian.bamboo.specs.builders.task.ScpTask;
import com.atlassian.bamboo.specs.codegen.emitters.fragment.SecretSetterEmitter;
import com.atlassian.bamboo.specs.codegen.emitters.value.EntityPropertiesEmitter;
import com.atlassian.bamboo.specs.model.task.ArtifactItemProperties;
import com.atlassian.bamboo.specs.model.task.ScpTaskProperties;
import org.jetbrains.annotations.NotNull;

public class ScpTaskEmitter extends EntityPropertiesEmitter<ScpTaskProperties> {

    @NotNull
    @Override
    public String emitCode(@NotNull CodeGenerationContext context, @NotNull ScpTaskProperties entity) throws CodeGenerationException {
        builderClass = ScpTask.class;
        fieldsToSkip.add("artifactItem");
        fieldsToSkip.add("localPath");
        fieldsToSkip.add("localPathAntStyle");
        fieldsToSkip.add("password");
        fieldsToSkip.add("key");
        fieldsToSkip.add("passphrase");

        return emitConstructorInvocation(context, entity)
                + emitFields(context, entity)
                + context.incIndentation()
                + context.newLine()
                + emitCodeForAuthentication(context, entity)
                + context.newLine()
                + emitCodeForLocalPathOrArtifact(context, entity)
                + context.decIndentation();
    }

    private String emitCodeForAuthentication(@NotNull CodeGenerationContext context, @NotNull ScpTaskProperties entity) {
        switch (entity.getAuthenticationType()) {
            case PASSWORD:
                return String.format(".authenticateWithPassword(%s)", SecretSetterEmitter.SECRET_PLACEHOLDER);
            case KEY_WITHOUT_PASSPHRASE:
                return String.format(".authenticateWithKey(%s)", SecretSetterEmitter.SECRET_PLACEHOLDER);
            case KEY_WITH_PASSPHRASE:
                return String.format(".authenticateWithKeyWithPassphrase(%s, %s)", SecretSetterEmitter.SECRET_PLACEHOLDER, SecretSetterEmitter.SECRET_PLACEHOLDER);
            default:
                return "";
        }
    }

    private String emitCodeForLocalPathOrArtifact(@NotNull CodeGenerationContext context, @NotNull ScpTaskProperties entity) throws CodeGenerationException {
        if (entity.getLocalPath() != null) {
            // local files or directories
            return emitCodeForLocalPath(entity);
        } else if (entity.getArtifactItem() != null) {
            // artifacts
            return emitCodeForArtifact(context, entity);
        } else {
            throw new CodeGenerationException("ScpTask requires an artifact to be uploaded. "
                    + "Neither a local file/directory nor a downloaded artifact was found in the task configuration.");
        }
    }

    private String emitCodeForLocalPath(@NotNull ScpTaskProperties entity) {
        return String.format(".fromLocalPath(\"%s\", %b)", entity.getLocalPath(), entity.isLocalPathAntStyle());
    }

    private String emitCodeForArtifact(@NotNull CodeGenerationContext context, @NotNull ScpTaskProperties entity)
            throws CodeGenerationException {
        final ArtifactItemProperties aip = entity.getArtifactItem();
        if (aip == null) {
            throw new CodeGenerationException("Artifact property is null");
        }

        return String.format(".fromArtifact(%s)", new ArtifactItemEmitter().emitCode(context, aip));
    }
}
