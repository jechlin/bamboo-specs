package com.atlassian.bamboo.specs.codegen.emitters.value;

import com.atlassian.bamboo.specs.api.codegen.CodeEmitter;
import com.atlassian.bamboo.specs.api.codegen.CodeGenerationException;
import com.atlassian.bamboo.specs.api.model.EntityProperties;
import com.atlassian.bamboo.specs.codegen.emitters.CodeGenerationUtils;
import org.jetbrains.annotations.NotNull;

import java.time.Duration;
import java.util.Collection;
import java.util.Map;

/**
 * Helper class that looks for best code generator for an object.
 */
public final class ValueEmitterFactory {
    private ValueEmitterFactory() {
    }

    public static CodeEmitter emitterFor(@NotNull EntityProperties entity) throws CodeGenerationException {
        CodeEmitter codeEmitter = CodeGenerationUtils.findEmitterByAnnotation(entity.getClass());
        if (codeEmitter != null) {
            return codeEmitter;
        }
        return new EntityPropertiesEmitter();
    }

    public static CodeEmitter emitterFor(@NotNull Object object) throws CodeGenerationException {
        if (object instanceof EntityProperties) {
            return emitterFor((EntityProperties) object);
        } else if (object instanceof Duration) {
            return new DurationEmitter();
        } else if (object instanceof Map) {
            return new MapEmitter();
        } else if (object instanceof Collection) {
            throw new CodeGenerationException("Generating code for collections is not supported yet");
        } else {
            return new LiteralEmitter();
        }
    }
}
