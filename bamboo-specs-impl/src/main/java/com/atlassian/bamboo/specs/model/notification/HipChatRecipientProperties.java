package com.atlassian.bamboo.specs.model.notification;

import com.atlassian.bamboo.specs.api.codegen.annotations.Secret;
import com.atlassian.bamboo.specs.api.model.AtlassianModuleProperties;
import com.atlassian.bamboo.specs.api.model.notification.NotificationRecipientProperties;
import com.atlassian.bamboo.specs.api.validators.common.ImporterUtils;
import com.atlassian.bamboo.specs.api.validators.common.ValidationContext;
import org.jetbrains.annotations.NotNull;

import javax.annotation.concurrent.Immutable;
import java.util.Objects;

@Immutable
public final class HipChatRecipientProperties extends NotificationRecipientProperties {

    private static final AtlassianModuleProperties ATLASSIAN_PLUGIN = new AtlassianModuleProperties("com.atlassian.bamboo.plugins.bamboo-hipchat:recipient.hipchat");

    @Secret
    private final String apiToken;
    private final String room;
    private final boolean notifyUsers;

    private HipChatRecipientProperties() {
        apiToken = null;
        room = null;
        notifyUsers = false;
    }

    public HipChatRecipientProperties(String apiToken, String room, boolean notifyUsers) {
        this.apiToken = apiToken;
        this.room = room;
        this.notifyUsers = notifyUsers;
        validate();
    }

    @NotNull
    public String getApiToken() {
        return apiToken;
    }

    @NotNull
    public String getRoom() {
        return room;
    }

    public boolean isNotifyUsers() {
        return notifyUsers;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        HipChatRecipientProperties that = (HipChatRecipientProperties) o;
        return isNotifyUsers() == that.isNotifyUsers() &&
                Objects.equals(getApiToken(), that.getApiToken()) &&
                Objects.equals(getRoom(), that.getRoom());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getApiToken(), getRoom(), isNotifyUsers());
    }

    @NotNull
    @Override
    public AtlassianModuleProperties getAtlassianPlugin() {
        return ATLASSIAN_PLUGIN;
    }

    @Override
    public void validate() {
        ValidationContext context = ValidationContext.of("hipChatRecipient");
        ImporterUtils.checkNotBlank(context, "apiToken", apiToken);
        ImporterUtils.checkNotBlank(context, "room", room);
    }
}
