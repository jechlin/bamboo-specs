package com.atlassian.bamboo.specs.model.notification;

import com.atlassian.bamboo.specs.api.builders.Applicability;
import com.atlassian.bamboo.specs.api.model.AtlassianModuleProperties;
import com.atlassian.bamboo.specs.api.model.notification.NotificationTypeProperties;
import com.atlassian.bamboo.specs.api.validators.common.ValidationContext;
import org.jetbrains.annotations.NotNull;

import java.util.EnumSet;
import java.util.Objects;
import java.util.Set;

import static com.atlassian.bamboo.specs.api.validators.common.ImporterUtils.checkPositive;

public class XFailedChainsNotificationProperties extends NotificationTypeProperties {

    public static final ValidationContext VALIDATION_CONTEXT = ValidationContext.of("XFailed Chains Notifications");
    public static final AtlassianModuleProperties ATLASSIAN_PLUGIN =
            new AtlassianModuleProperties("com.atlassian.bamboo.plugin.system.notifications:chainCompleted.XFailedChains");

    private final int numberOfFailures;

    private XFailedChainsNotificationProperties() {
        numberOfFailures = 1;
    }

    public XFailedChainsNotificationProperties(int numberOfFailures) {
        this.numberOfFailures = numberOfFailures;

        validate();
    }

    public int getNumberOfFailures() {
        return numberOfFailures;
    }

    @NotNull
    @Override
    public AtlassianModuleProperties getAtlassianPlugin() {
        return ATLASSIAN_PLUGIN;
    }

    @Override
    public void validate() {
        checkPositive(VALIDATION_CONTEXT, "numberOfFailures", numberOfFailures);
    }

    @Override
    public Set<Applicability> applicableTo() {
        return EnumSet.of(Applicability.PLANS);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        XFailedChainsNotificationProperties that = (XFailedChainsNotificationProperties) o;
        return numberOfFailures == that.numberOfFailures;
    }

    @Override
    public int hashCode() {
        return Objects.hash(numberOfFailures);
    }
}
