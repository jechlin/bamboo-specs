/**
 * Bitbucket Cloud repository type.
 */
package com.atlassian.bamboo.specs.model.repository.bitbucket.cloud;
