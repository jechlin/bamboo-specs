package com.atlassian.bamboo.specs.model.repository.git;

import com.atlassian.bamboo.specs.api.model.EntityProperties;

public interface AuthenticationProperties extends EntityProperties {
}
