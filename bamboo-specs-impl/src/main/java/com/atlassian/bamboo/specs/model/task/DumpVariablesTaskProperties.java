package com.atlassian.bamboo.specs.model.task;

import com.atlassian.bamboo.specs.api.model.AtlassianModuleProperties;
import com.atlassian.bamboo.specs.api.model.task.TaskProperties;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.annotation.concurrent.Immutable;

@Immutable
public final class DumpVariablesTaskProperties extends TaskProperties {

    private static final AtlassianModuleProperties ATLASSIAN_PLUGIN =
            new AtlassianModuleProperties("com.atlassian.bamboo.plugins.bamboo-variable-inject-plugin:dump");

    protected DumpVariablesTaskProperties() {
        super();
    }

    public DumpVariablesTaskProperties(@Nullable String description,
                                       boolean enabled) {
        super(description, enabled);

        validate();
    }

    @NotNull
    @Override
    public AtlassianModuleProperties getAtlassianPlugin() {
        return ATLASSIAN_PLUGIN;
    }

    @Override
    public void validate() {
        super.validate();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof DumpVariablesTaskProperties)) {
            return false;
        }
        return super.equals(o);
    }

    @Override
    public int hashCode() {
        return super.hashCode();
    }
}
