package com.atlassian.bamboo.specs.model.task;


import com.atlassian.bamboo.specs.api.exceptions.PropertiesValidationException;
import com.atlassian.bamboo.specs.api.model.AtlassianModuleProperties;
import com.atlassian.bamboo.specs.api.model.task.TaskProperties;
import com.atlassian.bamboo.specs.api.validators.common.ValidationContext;
import org.apache.commons.lang3.StringUtils;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.annotation.concurrent.Immutable;
import java.util.Objects;

import static com.atlassian.bamboo.specs.api.validators.common.ImporterUtils.checkThat;

@Immutable
public final class FastlaneTaskProperties extends TaskProperties {
    private static final AtlassianModuleProperties MODULE_KEY =
            new AtlassianModuleProperties("com.atlassian.bamboo.plugins.xcode.bamboo-xcode-plugin:fastlaneTaskType");

    @NotNull
    private String lane;
    @Nullable
    private String environmentVariables;
    @Nullable
    private String executableLabel;
    @Nullable
    private String workingSubdirectory;

    protected FastlaneTaskProperties() {
        super();
    }

    public FastlaneTaskProperties(@Nullable String description, boolean enabled,
                                  @NotNull String lane,
                                  @Nullable String environmentVariables,
                                  @NotNull String executableLabel,
                                  @Nullable String workingSubdirectory) throws PropertiesValidationException {
        super(description, enabled);
        this.lane = lane;
        this.environmentVariables = environmentVariables;
        this.executableLabel = executableLabel;
        this.workingSubdirectory = workingSubdirectory;

        validate();
    }

    @NotNull
    public String getLane() {
        return lane;
    }

    @Nullable
    public String getEnvironmentVariables() {
        return environmentVariables;
    }

    @Nullable
    public String getExecutableLabel() {
        return executableLabel;
    }

    @Nullable
    public String getWorkingSubdirectory() {
        return workingSubdirectory;
    }

    @NotNull
    @Override
    public AtlassianModuleProperties getAtlassianPlugin() {
        return MODULE_KEY;
    }

    @Override
    public void validate() {
        super.validate();

        final ValidationContext context = ValidationContext.of("Fastlane task");
        checkThat(context, StringUtils.isNotBlank(lane), "Lane is not defined");
        checkThat(context, StringUtils.isNotBlank(executableLabel), "Executable label is not defined");
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        if (!super.equals(o)) {
            return false;
        }
        FastlaneTaskProperties that = (FastlaneTaskProperties) o;
        return Objects.equals(lane, that.lane) &&
                Objects.equals(environmentVariables, that.environmentVariables) &&
                Objects.equals(executableLabel, that.executableLabel) &&
                Objects.equals(workingSubdirectory, that.workingSubdirectory);
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), lane, environmentVariables, executableLabel, workingSubdirectory);
    }
}
