package com.atlassian.bamboo.specs.model.task;

import com.atlassian.bamboo.specs.api.builders.Applicability;
import com.atlassian.bamboo.specs.api.exceptions.PropertiesValidationException;
import com.atlassian.bamboo.specs.api.model.AtlassianModuleProperties;
import com.atlassian.bamboo.specs.api.model.task.TaskProperties;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.annotation.concurrent.Immutable;
import java.util.EnumSet;
import java.util.Objects;

@Immutable
public class MavenDependenciesProcessorTaskProperties extends TaskProperties {

    private static final AtlassianModuleProperties MODULE_KEY =
            new AtlassianModuleProperties("com.atlassian.bamboo.plugins.maven:task.mvn.dependencies.processor");

    @Nullable
    private String overrideProjectFile;

    @Nullable
    private String workingSubdirectory;

    @Nullable
    private String alternatePathForTheGlobalSettingsFile;

    @Nullable
    private String alternatePathForTheUserSettingsFile;

    private String pathToMavenLocalRepository;

    @NotNull
    @Override
    public AtlassianModuleProperties getAtlassianPlugin() {
        return MODULE_KEY;
    }

    protected MavenDependenciesProcessorTaskProperties() {
        super();
    }

    public MavenDependenciesProcessorTaskProperties(String description, boolean enabled,
                                                    String overrideProjectFile, String workingSubdirectory,
                                                    String alternatePathForTheGlobalSettingsFile,
                                                    String alternatePathForTheUserSettingsFile,
                                                    String pathToMavenLocalRepository) throws PropertiesValidationException {
        super(description, enabled);
        this.overrideProjectFile = overrideProjectFile;
        this.workingSubdirectory = workingSubdirectory;
        this.alternatePathForTheGlobalSettingsFile = alternatePathForTheGlobalSettingsFile;
        this.alternatePathForTheUserSettingsFile = alternatePathForTheUserSettingsFile;
        this.pathToMavenLocalRepository = pathToMavenLocalRepository;
        validate();
    }

    @Nullable
    public String getOverrideProjectFile() {
        return overrideProjectFile;
    }

    @Nullable
    public String getWorkingSubdirectory() {
        return workingSubdirectory;
    }

    @Nullable
    public String getAlternatePathForTheGlobalSettingsFile() {
        return alternatePathForTheGlobalSettingsFile;
    }

    @Nullable
    public String getAlternatePathForTheUserSettingsFile() {
        return alternatePathForTheUserSettingsFile;
    }

    @Nullable
    public String getPathToMavenLocalRepository() {
        return pathToMavenLocalRepository;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        if (!super.equals(o)) {
            return false;
        }
        MavenDependenciesProcessorTaskProperties that = (MavenDependenciesProcessorTaskProperties) o;
        return Objects.equals(overrideProjectFile, that.overrideProjectFile) &&
                Objects.equals(workingSubdirectory, that.workingSubdirectory) &&
                Objects.equals(alternatePathForTheGlobalSettingsFile, that.alternatePathForTheGlobalSettingsFile) &&
                Objects.equals(alternatePathForTheUserSettingsFile, that.alternatePathForTheUserSettingsFile) &&
                Objects.equals(pathToMavenLocalRepository, that.pathToMavenLocalRepository);
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), overrideProjectFile, workingSubdirectory,
                alternatePathForTheGlobalSettingsFile, alternatePathForTheUserSettingsFile, pathToMavenLocalRepository);
    }

    @Override
    public void validate() {
        super.validate();
    }

    @Override
    public EnumSet<Applicability> applicableTo() {
        return EnumSet.of(Applicability.PLANS);
    }
}
