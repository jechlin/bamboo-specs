package com.atlassian.bamboo.specs.model.task;

import com.atlassian.bamboo.specs.api.builders.Applicability;
import com.atlassian.bamboo.specs.api.codegen.annotations.CodeGenerator;
import com.atlassian.bamboo.specs.api.exceptions.PropertiesValidationException;
import com.atlassian.bamboo.specs.api.model.AtlassianModuleProperties;
import com.atlassian.bamboo.specs.api.model.task.TaskProperties;
import com.atlassian.bamboo.specs.api.validators.common.ValidationContext;
import com.atlassian.bamboo.specs.builders.task.NUnitRunnerTask;
import com.atlassian.bamboo.specs.codegen.emitters.task.NUnitVersionEmitter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.ArrayList;
import java.util.EnumSet;
import java.util.List;
import java.util.Objects;

import static com.atlassian.bamboo.specs.api.validators.common.ImporterUtils.checkNotBlank;
import static com.atlassian.bamboo.specs.api.validators.common.ImporterUtils.checkNotNull;
import static java.util.Collections.unmodifiableList;

public class NUnitRunnerTaskProperties extends TaskProperties {
    private static final AtlassianModuleProperties NUNIT_ATLASSIAN_PLUGIN =
            new AtlassianModuleProperties("com.atlassian.bamboo.plugin.dotnet:nunitRunner");
    private static final ValidationContext VALIDATION_CONTEXT = ValidationContext.of("NUnit runner task");

    private final String executable;
    @CodeGenerator(NUnitVersionEmitter.class)
    private final NUnitRunnerTask.NUnitVersion nUnitVersion;
    private final String nUnitTestFiles;
    private final String resultFilename;
    private final List<String> testsToRun;
    private final List<String> testCategoriesToInclude;
    private final List<String> testCategoriesToExclude;
    @Nullable
    private final String commandLineOptions;
    @Nullable
    private final String environmentVariables;

    private NUnitRunnerTaskProperties() {
        this.executable = null;
        this.nUnitVersion = null;
        this.nUnitTestFiles = null;
        this.resultFilename = null;
        this.testsToRun = null;
        this.testCategoriesToInclude = null;
        this.testCategoriesToExclude = null;
        this.commandLineOptions = null;
        this.environmentVariables = null;
    }

    public NUnitRunnerTaskProperties(@Nullable String description, boolean enabled, @NotNull String executable,
                                     @NotNull NUnitRunnerTask.NUnitVersion nUnitVersion,
                                     @NotNull String nUnitTestFiles, @NotNull String resultFilename,
                                     @NotNull List<String> testsToRun, @NotNull List<String> testCategoriesToInclude,
                                     @NotNull List<String> testCategoriesToExclude, @Nullable String commandLineOptions,
                                     @Nullable String environmentVariables) throws PropertiesValidationException {
        super(description, enabled);
        this.executable = executable;
        this.nUnitVersion = nUnitVersion;
        this.nUnitTestFiles = nUnitTestFiles;
        this.resultFilename = resultFilename;
        this.testsToRun = unmodifiableList(new ArrayList<>(testsToRun));
        this.testCategoriesToInclude = unmodifiableList(new ArrayList(testCategoriesToInclude));
        this.testCategoriesToExclude = unmodifiableList(new ArrayList(testCategoriesToExclude));
        this.commandLineOptions = commandLineOptions;
        this.environmentVariables = environmentVariables;
        validate();
    }

    @NotNull
    @Override
    public AtlassianModuleProperties getAtlassianPlugin() {
        return NUNIT_ATLASSIAN_PLUGIN;
    }

    @NotNull
    public String getExecutable() {
        return executable;
    }

    @NotNull
    public NUnitRunnerTask.NUnitVersion getNUnitVersion() {
        return nUnitVersion;
    }

    @NotNull
    public String getNUnitTestFiles() {
        return nUnitTestFiles;
    }

    @NotNull
    public String getResultFilename() {
        return resultFilename;
    }

    @NotNull
    public List<String> getTestsToRun() {
        return testsToRun;
    }

    @NotNull
    public List<String> getTestCategoriesToInclude() {
        return testCategoriesToInclude;
    }

    @NotNull
    public List<String> getTestCategoriesToExclude() {
        return testCategoriesToExclude;
    }

    @Nullable
    public String getCommandLineOptions() {
        return commandLineOptions;
    }

    @Nullable
    public String getEnvironmentVariables() {
        return environmentVariables;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        if (!super.equals(o)) {
            return false;
        }
        NUnitRunnerTaskProperties that = (NUnitRunnerTaskProperties) o;
        return Objects.equals(executable, that.executable) &&
                Objects.equals(nUnitVersion, that.nUnitVersion) &&
                Objects.equals(nUnitTestFiles, that.nUnitTestFiles) &&
                Objects.equals(resultFilename, that.resultFilename) &&
                Objects.equals(testsToRun, that.testsToRun) &&
                Objects.equals(testCategoriesToInclude, that.testCategoriesToInclude) &&
                Objects.equals(testCategoriesToExclude, that.testCategoriesToExclude) &&
                Objects.equals(commandLineOptions, that.commandLineOptions) &&
                Objects.equals(environmentVariables, that.environmentVariables);
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), executable, nUnitVersion, nUnitTestFiles, resultFilename, testsToRun, testCategoriesToInclude, testCategoriesToExclude, commandLineOptions, environmentVariables);
    }

    @Override
    public void validate() {
        super.validate();
        checkNotBlank(VALIDATION_CONTEXT, "executable", executable);
        checkNotNull(VALIDATION_CONTEXT, "nUnitVersion", nUnitVersion);
        checkNotBlank(VALIDATION_CONTEXT, "nUnitTestFiles", nUnitTestFiles);
        checkNotBlank(VALIDATION_CONTEXT, "resultFilename", resultFilename);
        checkNotNull(VALIDATION_CONTEXT, "testsToRun", testsToRun);
        checkNotNull(VALIDATION_CONTEXT, "testCategoriesToInclude", testCategoriesToInclude);
        checkNotNull(VALIDATION_CONTEXT, "testCategoriesToExclude", testCategoriesToExclude);
    }

    @Override
    public EnumSet<Applicability> applicableTo() {
        return EnumSet.of(Applicability.PLANS);
    }

}
