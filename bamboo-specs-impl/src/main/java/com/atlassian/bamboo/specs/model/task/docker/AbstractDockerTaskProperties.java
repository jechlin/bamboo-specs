package com.atlassian.bamboo.specs.model.task.docker;


import com.atlassian.bamboo.specs.api.exceptions.PropertiesValidationException;
import com.atlassian.bamboo.specs.api.model.AtlassianModuleProperties;
import com.atlassian.bamboo.specs.api.model.task.TaskProperties;
import org.apache.commons.lang3.StringUtils;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Objects;

public abstract class AbstractDockerTaskProperties extends TaskProperties {
    public static final AtlassianModuleProperties MODULE_KEY =
            new AtlassianModuleProperties("com.atlassian.bamboo.plugins.bamboo-docker-plugin:task.docker.cli");

    @Nullable
    protected String environmentVariables;
    @Nullable
    protected String workingSubdirectory;

    protected AbstractDockerTaskProperties() {
        super();
    }

    public AbstractDockerTaskProperties(@Nullable String description,
                                        boolean enabled,
                                        @Nullable String environmentVariables,
                                        @Nullable String workingSubdirectory) throws PropertiesValidationException {
        super(description, enabled);

        this.environmentVariables = StringUtils.defaultString(environmentVariables);
        this.workingSubdirectory = StringUtils.defaultString(workingSubdirectory);
    }

    @Nullable
    public String getEnvironmentVariables() {
        return environmentVariables;
    }

    @Nullable
    public String getWorkingSubdirectory() {
        return workingSubdirectory;
    }

    @NotNull
    @Override
    public AtlassianModuleProperties getAtlassianPlugin() {
        return MODULE_KEY;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        if (!super.equals(o)) {
            return false;
        }
        AbstractDockerTaskProperties that = (AbstractDockerTaskProperties) o;
        return Objects.equals(environmentVariables, that.environmentVariables) &&
                Objects.equals(workingSubdirectory, that.workingSubdirectory);
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), environmentVariables, workingSubdirectory);
    }
}
