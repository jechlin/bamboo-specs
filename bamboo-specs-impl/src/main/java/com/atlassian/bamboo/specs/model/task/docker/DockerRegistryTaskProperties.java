package com.atlassian.bamboo.specs.model.task.docker;

import com.atlassian.bamboo.specs.api.codegen.annotations.Builder;
import com.atlassian.bamboo.specs.api.codegen.annotations.CodeGenerator;
import com.atlassian.bamboo.specs.api.exceptions.PropertiesValidationException;
import com.atlassian.bamboo.specs.api.validators.common.ValidationContext;
import com.atlassian.bamboo.specs.builders.task.DockerPushImageTask;
import com.atlassian.bamboo.specs.codegen.emitters.task.DockerRegistryEmitter;
import org.apache.commons.lang3.StringUtils;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.annotation.concurrent.Immutable;
import java.util.Objects;

import static com.atlassian.bamboo.specs.api.validators.common.ImporterUtils.checkNotBlank;
import static com.atlassian.bamboo.specs.api.validators.common.ImporterUtils.checkNotNull;

@Builder(DockerPushImageTask.class)
@CodeGenerator(DockerRegistryEmitter.class)
@Immutable
public class DockerRegistryTaskProperties extends AbstractDockerTaskProperties {

    public static final ValidationContext VALIDATION_CONTEXT = ValidationContext.of("Docker Push/Pull image task:");

    public enum OperationType {
        PUSH,
        PULL;
    }

    public enum RegistryType {
        DOCKER_HUB,
        CUSTOM
    }

    @NotNull
    private OperationType operationType;
    @NotNull
    private String image;
    @NotNull
    private RegistryType registryType;
    @Nullable
    private String username;
    @Nullable
    private String password;
    @Nullable
    private String email;


    protected DockerRegistryTaskProperties() {
        super();
    }

    public DockerRegistryTaskProperties(@Nullable String description, boolean enabled,
                                        @NotNull OperationType operationType,
                                        @NotNull String image,
                                        @NotNull RegistryType registryType,
                                        @Nullable String username,
                                        @Nullable String password,
                                        @Nullable String email,
                                        @Nullable String environmentVariables,
                                        @Nullable String workingSubdirectory) throws PropertiesValidationException {
        super(description, enabled, environmentVariables, workingSubdirectory);


        this.operationType = checkNotNull(VALIDATION_CONTEXT, "operationType", operationType);
        this.registryType = checkNotNull(VALIDATION_CONTEXT, "registryType", registryType);

        this.image = image;
        this.username = username;
        this.password = password;
        this.email = email;

        validate();
    }

    @NotNull
    public String getImage() {
        return image;
    }

    @Nullable
    public String getUsername() {
        return username;
    }

    @Nullable
    public String getPassword() {
        return password;
    }

    @Nullable
    public String getEmail() {
        return email;
    }

    @NotNull
    public OperationType getOperationType() {
        return operationType;
    }

    @NotNull
    public RegistryType getRegistryType() {
        return registryType;
    }

    @Override
    public void validate() {
        super.validate();
        checkNotBlank(VALIDATION_CONTEXT, "repository", image);

        if (StringUtils.isNotBlank(username) || StringUtils.isNotBlank(password) || StringUtils.isNotBlank(email)) {
            checkNotBlank(VALIDATION_CONTEXT, "username", username);
            checkNotBlank(VALIDATION_CONTEXT, "password", password);
            checkNotBlank(VALIDATION_CONTEXT, "email", email);
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        DockerRegistryTaskProperties that = (DockerRegistryTaskProperties) o;
        return operationType == that.operationType &&
                Objects.equals(image, that.image) &&
                registryType == that.registryType &&
                Objects.equals(username, that.username) &&
                Objects.equals(password, that.password) &&
                Objects.equals(email, that.email);
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), operationType, image, registryType, username, password, email);
    }
}
