package com.atlassian.bamboo.specs.model.trigger;

import com.atlassian.bamboo.specs.api.builders.Applicability;
import com.atlassian.bamboo.specs.api.builders.AtlassianModule;
import com.atlassian.bamboo.specs.api.codegen.annotations.CodeGenerator;
import com.atlassian.bamboo.specs.api.codegen.annotations.SkipCodeGen;
import com.atlassian.bamboo.specs.api.exceptions.PropertiesValidationException;
import com.atlassian.bamboo.specs.api.model.AtlassianModuleProperties;
import com.atlassian.bamboo.specs.api.model.trigger.TriggerProperties;
import com.atlassian.bamboo.specs.api.util.EntityPropertiesBuilders;
import com.atlassian.bamboo.specs.api.validators.CronExpressionClientSideValidator;
import com.atlassian.bamboo.specs.codegen.emitters.trigger.ScheduledTriggerEmitter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.annotation.concurrent.Immutable;
import java.util.EnumSet;
import java.util.Objects;

@Immutable
@CodeGenerator(ScheduledTriggerEmitter.class)
public final class ScheduledTriggerProperties extends TriggerProperties {
    public static final String MODULE_KEY = "com.atlassian.bamboo.triggers.atlassian-bamboo-triggers:schedule";
    private static final AtlassianModuleProperties ATLASSIAN_MODULE = EntityPropertiesBuilders.build(new AtlassianModule(MODULE_KEY));

    public enum Container {DEPLOYMENT, PLAN}

    private final String cronExpression;
    private final String artifactBranch;

    //required for code generator to produce different builders from same properties class
    @SkipCodeGen
    private final Container container;

    private ScheduledTriggerProperties() {
        super();
        this.cronExpression = "0 0 0 ? * *";
        this.artifactBranch = null;
        container = Container.PLAN;
    }

    public ScheduledTriggerProperties(final String name,
                                      final String description,
                                      final boolean isEnabled,
                                      @NotNull final String cronExpression,
                                      @Nullable final String artifactBranch,
                                      @NotNull Container container) throws PropertiesValidationException {
        super(name, description, isEnabled);
        this.cronExpression = cronExpression.trim();
        this.artifactBranch = artifactBranch;
        this.container = container;
        validate();
    }

    @NotNull
    @Override
    public AtlassianModuleProperties getAtlassianPlugin() {
        return ATLASSIAN_MODULE;
    }

    public String getCronExpression() {
        return cronExpression;
    }

    public String getArtifactBranch() {
        return artifactBranch;
    }

    public Container getContainer() {
        return container;
    }

    @Override
    public void validate() throws PropertiesValidationException {
        super.validate();
        CronExpressionClientSideValidator.validate(cronExpression);
    }


    @Override
    public EnumSet<Applicability> applicableTo() {
        return container == Container.PLAN
                ? EnumSet.of(Applicability.PLANS)
                : EnumSet.of(Applicability.DEPLOYMENTS);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        if (!super.equals(o)) {
            return false;
        }
        ScheduledTriggerProperties that = (ScheduledTriggerProperties) o;
        return Objects.equals(cronExpression, that.cronExpression) &&
                Objects.equals(artifactBranch, that.artifactBranch) &&
                container == that.container;
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), cronExpression, artifactBranch, container);
    }
}
