package com.atlassian.bamboo.specs.util;

import com.atlassian.bamboo.specs.api.model.EntityProperties;

import java.util.Objects;

/**
 * Wrapper for root entity that is sent to Bamboo.
 * It's main purpose for now it to hold model version.
 * It will be used by Bamboo Server to reject incompatible versions in the future.
 */
public class BambooSpecProperties implements EntityProperties {
    private final String specModelVersion;

    private final EntityProperties rootEntity;

    private BambooSpecProperties() {
        specModelVersion = BambooSpecVersion.getModelVersion();
        rootEntity = null;
    }

    public BambooSpecProperties(final EntityProperties rootEntity) {
        specModelVersion = BambooSpecVersion.getModelVersion();

        this.rootEntity = rootEntity;
    }

    public String getSpecModelVersion() {
        return specModelVersion;
    }

    public EntityProperties getRootEntity() {
        return rootEntity;
    }

    @Override
    public void validate() {
        //    root entity is already validated when build in constructor
    }

    @Override
    public boolean equals(final Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final BambooSpecProperties that = (BambooSpecProperties) o;
        return getSpecModelVersion() == that.getSpecModelVersion() &&
                Objects.equals(getRootEntity(), that.getRootEntity());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getSpecModelVersion(), getRootEntity());
    }
}
