package com.atlassian.bamboo.specs.util;

import com.atlassian.bamboo.specs.api.builders.EntityPropertiesBuilder;
import com.atlassian.bamboo.specs.api.model.EntityProperties;
import com.atlassian.bamboo.specs.api.util.EntityPropertiesBuilders;

import java.io.Writer;

/**
 * Wrapper to convert bamboo spec object to String.
 */
public final class BambooSpecSerializer {
    private BambooSpecSerializer() {
    }

    public static String dump(EntityPropertiesBuilder entity) {
        final EntityProperties entityProperties = EntityPropertiesBuilders.build(entity);
        final BambooSpecProperties bambooSpecProperties = new BambooSpecProperties(entityProperties);
        return Yamlizator.getYaml().dump(bambooSpecProperties);
    }

    public static void dump(EntityPropertiesBuilder entity, Writer writer) {
        final EntityProperties entityProperties = EntityPropertiesBuilders.build(entity);
        final BambooSpecProperties bambooSpecProperties = new BambooSpecProperties(entityProperties);
        Yamlizator.getYaml().dump(bambooSpecProperties, writer);
    }
}
