package com.atlassian.bamboo.specs.util;

import com.atlassian.bamboo.specs.api.exceptions.PropertiesValidationException;
import org.jetbrains.annotations.NotNull;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;

import static com.atlassian.bamboo.specs.api.validators.common.ImporterUtils.checkNotNull;

public final class FileUtils {

    private FileUtils() {

    }

    /**
     * Read file content.
     * @param path to file
     * @param fileIsNullErrorMessage error message thrown when file is null
     * @param fileReadErrorMessage error message thrown when can't read file
     * @return file body
     */
    public static String readFileContent(@NotNull Path path, String fileIsNullErrorMessage, String fileReadErrorMessage) {
        checkNotNull(fileIsNullErrorMessage, path);
        File file = path.toFile();
        if (!file.exists()) {
            throw new PropertiesValidationException(String.format("File %s does not exist", file.getAbsolutePath()));
        }
        try {
            return new String(Files.readAllBytes(path));
        } catch (final IOException e) {
            throw new PropertiesValidationException(String.format(fileReadErrorMessage, file.getAbsolutePath()), e);
        }
    }
}
