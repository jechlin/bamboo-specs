package com.atlassian.bamboo.specs.util;

import java.util.LinkedHashMap;
import java.util.Map;

public class MapBuilder<K, V> {
    private final LinkedHashMap<K, V> map = new LinkedHashMap<K, V>();

    /**
     * Puts value to the map.
     * @deprecated use {@link #put(Object, Object)}
     */
    @Deprecated
    public MapBuilder<K, V> append(K k, V v) {
        return this.put(k, v);
    }

    /**
     * Puts value to the map.
     */
    public MapBuilder<K, V> put(K k, V v) {
        map.put(k, v);
        return this;
    }

    public Map<K, V> build() {
        return map;
    }
}
