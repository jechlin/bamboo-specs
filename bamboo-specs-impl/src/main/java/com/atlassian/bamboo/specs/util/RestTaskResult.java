package com.atlassian.bamboo.specs.util;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public final class RestTaskResult {
    private final Exception exception;
    private final Object result;

    private RestTaskResult(@Nullable Object result, @Nullable Exception exception) {
        this.result = result;
        this.exception = exception;
    }

    @Nullable
    public static RestTaskResult forResult(@NotNull Object result) {
        return new RestTaskResult(result, null);
    }

    @Nullable
    public static RestTaskResult forException(@NotNull Exception exception) {
        return new RestTaskResult(null, exception);
    }

    @Nullable
    public Exception getException() {
        return exception;
    }

    @Nullable
    public Object getResult() {
        return result;
    }
}
