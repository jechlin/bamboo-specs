package com.atlassian.bamboo.specs.builders.notification;

import com.atlassian.bamboo.specs.api.exceptions.PropertiesValidationException;
import com.atlassian.bamboo.specs.api.util.EntityPropertiesBuilders;
import com.atlassian.bamboo.specs.model.notification.XFailedChainsNotificationProperties;
import junitparams.JUnitParamsRunner;
import junitparams.Parameters;
import org.junit.Test;
import org.junit.runner.RunWith;

import static org.junit.Assert.assertEquals;

@RunWith(JUnitParamsRunner.class)
public class XFailedChainsNotificationTest {

    @Test
    public void testHappyPath() throws Exception {
        XFailedChainsNotification notification = new XFailedChainsNotification();

        XFailedChainsNotificationProperties properties = EntityPropertiesBuilders.build(notification);

        assertEquals(1, properties.getNumberOfFailures());
    }

    @Parameters(method = "failureParameters")
    @Test(expected = PropertiesValidationException.class)
    public void testFailures(int numberOfFailures) throws Exception {
        XFailedChainsNotification notification = new XFailedChainsNotification();
        notification.numberOfFailures(numberOfFailures);

        XFailedChainsNotificationProperties properties = EntityPropertiesBuilders.build(notification);
    }

    private Object[] failureParameters() {
        return new Object[]{0, -1};
    }
}