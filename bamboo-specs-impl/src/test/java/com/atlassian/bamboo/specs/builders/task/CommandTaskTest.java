package com.atlassian.bamboo.specs.builders.task;

import com.atlassian.bamboo.specs.api.exceptions.PropertiesValidationException;
import com.atlassian.bamboo.specs.model.task.CommandTaskProperties;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import static org.hamcrest.core.IsEqual.equalTo;
import static org.junit.Assert.assertThat;

public class CommandTaskTest {

    @Rule
    public ExpectedException exception = ExpectedException.none();

    @Test
    public void testEqualsOnAllProperties() throws Exception {
        assertThat(new CommandTask()
                        .description("hello command 1")
                        .enabled(true)
                        .executable("pwd")
                        .argument("arg1 arg2 arg3")
                        .environmentVariables("SOME_HOME=/home ANOTHER_HOME=/home")
                        .workingSubdirectory("work/dir")
                        .build(),
                equalTo(new CommandTaskProperties(
                        "hello command 1",
                        true,
                        "pwd",
                        "arg1 arg2 arg3",
                        "SOME_HOME=/home ANOTHER_HOME=/home",
                        "work/dir"))
        );
    }

    @Test
    public void testMinimalConfiguration() throws Exception {
        new CommandTask()
                .executable("foo")
                .build();
    }

    @Test
    public void testValidation() throws Exception {
        exception.expect(PropertiesValidationException.class);
        exception.expectMessage("Command task: Executable is not defined");
        new CommandTask()
                .build();
    }

}