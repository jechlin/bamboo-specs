package com.atlassian.bamboo.specs.builders.task;

import com.atlassian.bamboo.specs.api.exceptions.PropertiesValidationException;
import com.atlassian.bamboo.specs.model.task.docker.DockerBuildImageTaskProperties;
import org.junit.Test;

import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.isEmptyOrNullString;
import static org.junit.Assert.assertThat;

public class DockerBuildImageTaskTest {

    private static final String IMAGE_NAME = "registry.address:port/some/namespace/image:latest";
    private static final String DOCKERFILE = "FROM ubuntu";
    private static final String FILENAME_TXT = "filename.txt";
    private static final String ENVIRONMENT_VARIABLES = "JAVA_HOME=/opt/java6";
    private static final String SUBDIRECTORY = "project/";

    @Test
    public void testMinimal() throws Exception {
        DockerBuildImageTaskProperties properties = new DockerBuildImageTask()
                .imageName(IMAGE_NAME)
                .dockerfileInWorkingDir()
                .build();

        assertThat(properties.getDockerfileContent(), is(DockerBuildImageTaskProperties.DockerfileContent.WORKING_DIR));
        assertThat(properties.getImageName(), equalTo(IMAGE_NAME));
        assertThat(properties.getDockerfile(), isEmptyOrNullString());
        assertThat(properties.isUseCache(), equalTo(true));
        assertThat(properties.isSaveAsFile(), equalTo(false));
        assertThat(properties.getImageFilename(), isEmptyOrNullString());
    }

    @Test
    public void testFull() throws Exception {
        DockerBuildImageTaskProperties properties = new DockerBuildImageTask()
                .imageName(IMAGE_NAME)
                .dockerfile(DOCKERFILE)
                .useCache(false)
                .saveAsFile(true)
                .imageFilename(FILENAME_TXT)
                .environmentVariables(ENVIRONMENT_VARIABLES)
                .workingSubdirectory(SUBDIRECTORY)
                .build();

        assertThat(properties.getDockerfileContent(), is(DockerBuildImageTaskProperties.DockerfileContent.INLINE));
        assertThat(properties.getImageName(), equalTo(IMAGE_NAME));
        assertThat(properties.getDockerfile(), equalTo(DOCKERFILE));
        assertThat(properties.isUseCache(), equalTo(false));
        assertThat(properties.isSaveAsFile(), equalTo(true));
        assertThat(properties.getImageFilename(), equalTo(FILENAME_TXT));
        assertThat(properties.getEnvironmentVariables(), equalTo(ENVIRONMENT_VARIABLES));
        assertThat(properties.getWorkingSubdirectory(), equalTo(SUBDIRECTORY));
    }

    @Test(expected = PropertiesValidationException.class)
    public void testValidateNoImageName() throws Exception {
        DockerBuildImageTaskProperties properties = new DockerBuildImageTask()
                .dockerfile(DOCKERFILE)
                .useCache(false)
                .saveAsFile(true)
                .imageFilename(FILENAME_TXT)
                .build();
    }

    @Test(expected = PropertiesValidationException.class)
    public void testValidateNoImageFilename() throws Exception {
        DockerBuildImageTaskProperties properties = new DockerBuildImageTask()
                .imageName(IMAGE_NAME)
                .dockerfile(DOCKERFILE)
                .useCache(false)
                .saveAsFile(true)
                .build();
    }

    @Test(expected = PropertiesValidationException.class)
    public void testValidateNoDockerfilecontent() throws Exception {
        DockerBuildImageTaskProperties properties = new DockerBuildImageTask()
                .imageName(IMAGE_NAME)
                .dockerfile(null)
                .useCache(false)
                .saveAsFile(true)
                .build();
    }
}