package com.atlassian.bamboo.specs.builders.task;

import com.atlassian.bamboo.specs.api.exceptions.PropertiesValidationException;
import com.atlassian.bamboo.specs.model.task.FastlaneTaskProperties;
import org.junit.Test;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;

public class FastlaneTaskTest {
    @Test
    public void testBuilderOK() {
        final String lane = "ios test";
        final String environment = "some:environment";
        final String directory = "directory";
        final String executableLabel = "Fastlane";

        final FastlaneTask fastlaneBuilder = new FastlaneTask()
                .lane(lane)
                .executableLabel(executableLabel)
                .environmentVariables(environment)
                .workingSubdirectory(directory);

        final FastlaneTaskProperties fastlaneTaskProperties = fastlaneBuilder.build();
        assertThat(fastlaneTaskProperties.getLane(), is(lane));
        assertThat(fastlaneTaskProperties.getExecutableLabel(), is(executableLabel));
        assertThat(fastlaneTaskProperties.getEnvironmentVariables(), is(environment));
        assertThat(fastlaneTaskProperties.getWorkingSubdirectory(), is(directory));
        assertThat(fastlaneTaskProperties.getAtlassianPlugin().getCompleteModuleKey(), is("com.atlassian.bamboo.plugins.xcode.bamboo-xcode-plugin:fastlaneTaskType"));
    }

    @Test(expected = PropertiesValidationException.class)
    public void testValidationFails() {
        final String environment = "some:environment";
        final String directory = "directory";

        final FastlaneTask fastlaneBuilder = new FastlaneTask()
                .environmentVariables(environment)
                .executableLabel("")
                .lane("")
                .workingSubdirectory(directory);

        final FastlaneTaskProperties fastlaneTaskProperties = fastlaneBuilder.build();
    }

}