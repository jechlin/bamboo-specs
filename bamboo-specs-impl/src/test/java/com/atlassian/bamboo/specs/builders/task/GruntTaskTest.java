package com.atlassian.bamboo.specs.builders.task;

import com.atlassian.bamboo.specs.api.exceptions.PropertiesValidationException;
import com.atlassian.bamboo.specs.model.task.GruntTaskProperties;
import org.junit.Test;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

public class GruntTaskTest {
    @Test(expected = PropertiesValidationException.class)
    public void testNodeExecutableIsRequired() {
        new GruntTask()
                .gruntCliExecutable("node_modules/grunt-cli/bin/grunt")
                .build();
    }

    @Test(expected = PropertiesValidationException.class)
    public void testGruntCliExecutableIsRequired() {
        new GruntTask()
                .nodeExecutable("Node.js 4.2")
                .gruntCliExecutable("")
                .build();
    }

    @Test
    public void testMinimalConfigurationWorks() {
        // should not throw exception
        new GruntTask()
                .nodeExecutable("Node.js 6")
                .build();
    }

    @Test
    public void testAllParamsAreSetWhenBuildingProperties() {
        final String description = "execute Gulp";
        final boolean taskEnabled = false;
        final String nodeExecutable = "Node.js 6.0";
        final String gruntExecutable = "node_modules/grunt-cli/bin/grunt";
        final String task = "test";
        final String gruntfile = "Gruntfile.js";
        final String environmentVariables = "NODE_HOME=/tmp/home";
        final String workingSubdirectory = "plugin";

        final GruntTask GruntTask = new GruntTask()
                .description(description)
                .enabled(taskEnabled)
                .nodeExecutable(nodeExecutable)
                .gruntCliExecutable(gruntExecutable)
                .task(task)
                .gruntfile(gruntfile)
                .environmentVariables(environmentVariables)
                .workingSubdirectory(workingSubdirectory);

        final GruntTaskProperties expectedProperties = new GruntTaskProperties(
                description,
                taskEnabled,
                nodeExecutable,
                environmentVariables,
                workingSubdirectory,
                gruntExecutable,
                task,
                gruntfile);

        assertThat(GruntTask.build(), is(equalTo(expectedProperties)));
    }
}
