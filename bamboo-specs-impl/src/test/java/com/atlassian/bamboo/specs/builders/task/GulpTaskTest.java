package com.atlassian.bamboo.specs.builders.task;

import com.atlassian.bamboo.specs.api.exceptions.PropertiesValidationException;
import com.atlassian.bamboo.specs.model.task.GulpTaskProperties;
import org.junit.Test;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

public class GulpTaskTest {
    @Test(expected = PropertiesValidationException.class)
    public void testNodeExecutableIsRequired() {
        new GulpTask()
                .gulpExecutable("node_modules/gulp/bin/gulp.js")
                .build();
    }

    @Test(expected = PropertiesValidationException.class)
    public void testGulpExecutableIsRequired() {
        new GulpTask()
                .nodeExecutable("Node.js 4.2")
                .gulpExecutable("")
                .build();
    }

    @Test
    public void testMinimalConfigurationWorks() {
        // should not throw exception
        new GulpTask()
                .nodeExecutable("Node.js 6")
                .build();
    }

    @Test
    public void testAllParamsAreSetWhenBuildingProperties() {
        final String description = "execute Gulp";
        final boolean taskEnabled = false;
        final String nodeExecutable = "Node.js 6.0";
        final String gulpExecutable = "node_modules/gulp/bin/gulp.js";
        final String task = "test";
        final String gulpfile = "gulpfile.js";
        final String environmentVariables = "NODE_HOME=/tmp/home";
        final String workingSubdirectory = "plugin";

        final GulpTask gulpTask = new GulpTask()
                .description(description)
                .enabled(taskEnabled)
                .nodeExecutable(nodeExecutable)
                .gulpExecutable(gulpExecutable)
                .task(task)
                .gulpfile(gulpfile)
                .environmentVariables(environmentVariables)
                .workingSubdirectory(workingSubdirectory);

        final GulpTaskProperties expectedProperties = new GulpTaskProperties(
                description,
                taskEnabled,
                nodeExecutable,
                environmentVariables,
                workingSubdirectory,
                gulpExecutable,
                task,
                gulpfile);

        assertThat(gulpTask.build(), is(equalTo(expectedProperties)));
    }
}
