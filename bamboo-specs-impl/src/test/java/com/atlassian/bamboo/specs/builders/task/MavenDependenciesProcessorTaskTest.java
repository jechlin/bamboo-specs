package com.atlassian.bamboo.specs.builders.task;

import com.atlassian.bamboo.specs.model.task.MavenDependenciesProcessorTaskProperties;
import org.hamcrest.Matchers;
import org.junit.Test;

import static org.junit.Assert.assertThat;

public class MavenDependenciesProcessorTaskTest {

    @Test
    public void shouldValidateMinimalConfiguration() {
        new MavenDependenciesProcessorTask().build();
    }

    @Test
    public void shouldBeEqualOnProperties() {
        assertThat(
            new MavenDependenciesProcessorTask()
                    .description("description")
                    .enabled(false)
                    .overrideProjectFile("overrideProjectFile")
                    .workingSubdirectory("workingSubdirectory")
                    .alternatePathForTheGlobalSettingsFile("alternatePathForTheGlobalSettingsFile")
                    .alternatePathForTheUserSettingsFile("alternatePathForTheUserSettingsFile")
                    .pathToMavenLocalRepository("pathToMavenLocalRepository")
                    .build(),
                Matchers.equalTo(new MavenDependenciesProcessorTaskProperties(
                        "description", false,
                        "overrideProjectFile", "workingSubdirectory",
                        "alternatePathForTheGlobalSettingsFile",
                        "alternatePathForTheUserSettingsFile",
                        "pathToMavenLocalRepository")));
    }

}