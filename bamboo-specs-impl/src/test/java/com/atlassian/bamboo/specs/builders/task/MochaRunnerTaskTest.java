package com.atlassian.bamboo.specs.builders.task;

import com.atlassian.bamboo.specs.api.exceptions.PropertiesValidationException;
import com.atlassian.bamboo.specs.model.task.MochaRunnerTaskProperties;
import org.junit.Test;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

public class MochaRunnerTaskTest {
    @Test(expected = PropertiesValidationException.class)
    public void testNodeExecutableIsRequired() {
        new MochaRunnerTask()
                .mochaExecutable("node_modules/mocha/bin/mocha")
                .build();
    }

    @Test(expected = PropertiesValidationException.class)
    public void testTestDirectoriesAreRequired() {
        new MochaRunnerTask()
                .nodeExecutable("Node.js 4.2")
                .mochaExecutable("node_modules/mocha/bin/mocha")
                .testFilesAndDirectories("")
                .build();
    }

    @Test(expected = PropertiesValidationException.class)
    public void testMochaExecutableIsRequired() {
        new MochaRunnerTask()
                .nodeExecutable("Node.js 4.2")
                .mochaExecutable("")
                .build();
    }

    @Test
    public void testMinimalConfigurationWorks() {
        // should not throw exception
        new MochaRunnerTask()
                .nodeExecutable("Node.js 6")
                .mochaExecutable("node_modules/mocha/bin/mocha")
                .build();
    }

    @Test
    public void testAllParamsAreSetWhenBuildingProperties() {
        final String description = "execute Gulp";
        final boolean taskEnabled = false;
        final String nodeExecutable = "Node.js 6.0";
        final String mochaExecutable = "node_modules/mocha/bin/mocha";
        final String testFilesAndDirectories = "test/ tests.js";
        final boolean parseTestResults = false;
        final String arguments = "--run-fast";
        final String environmentVariables = "NODE_HOME=/tmp/home";
        final String workingSubdirectory = "plugin";

        final MochaRunnerTask MochaRunnerTask = new MochaRunnerTask()
                .description(description)
                .enabled(taskEnabled)
                .nodeExecutable(nodeExecutable)
                .mochaExecutable(mochaExecutable)
                .testFilesAndDirectories(testFilesAndDirectories)
                .parseTestResults(parseTestResults)
                .arguments(arguments)
                .environmentVariables(environmentVariables)
                .workingSubdirectory(workingSubdirectory);

        final MochaRunnerTaskProperties expectedProperties = new MochaRunnerTaskProperties(
                description,
                taskEnabled,
                nodeExecutable,
                environmentVariables,
                workingSubdirectory,
                mochaExecutable,
                testFilesAndDirectories,
                parseTestResults,
                arguments);

        assertThat(MochaRunnerTask.build(), is(equalTo(expectedProperties)));
    }
}
