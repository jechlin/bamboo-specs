package com.atlassian.bamboo.specs.builders.task;

import com.atlassian.bamboo.specs.api.exceptions.PropertiesValidationException;
import com.atlassian.bamboo.specs.model.task.NUnitRunnerTaskProperties;
import org.hamcrest.Matchers;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import static java.util.Collections.emptyList;
import static java.util.Collections.singletonList;
import static org.junit.Assert.assertThat;


public class NUnitRunnerTaskTest {

    @Rule
    public ExpectedException exception = ExpectedException.none();

    @Test
    public void testValidTaskDefinition() {
        assertThat(new NUnitRunnerTask().executable("exec")
                        .nUnitVersion2()
                        .nUnitTestFiles("files")
                        .resultFilename("result.xml")
                        .testsToRun("ccc")
                        .testCategoriesToInclude("aaa")
                        .testCategoriesToExclude("bbb")
                        .commandLineOptions("options")
                        .environmentVariables("variables")
                        .description("description")
                        .enabled(false)
                        .build(),
                Matchers.equalTo(new NUnitRunnerTaskProperties("description", false, "exec", NUnitRunnerTask.NUnitVersion.NUNIT_2, "files", "result.xml",
                        singletonList("ccc"), singletonList("aaa"), singletonList("bbb"), "options", "variables")));
    }

    @Test
    public void testMinimalTaskDefinition() {
        assertThat(new NUnitRunnerTask().executable("exec")
                        .nUnitVersion2()
                        .nUnitTestFiles("files")
                        .resultFilename("result.xml")
                        .description("description")
                        .build(),
                Matchers.equalTo(new NUnitRunnerTaskProperties("description", true, "exec",
                        NUnitRunnerTask.NUnitVersion.NUNIT_2, "files", "result.xml",
                        emptyList(), emptyList(), emptyList(), null, null)));
    }

    @Test
    public void testValidation() {
        exception.expect(PropertiesValidationException.class);
        exception.expectMessage("executable");
        new NUnitRunnerTask()
                .nUnitTestFiles("files")
                .resultFilename("result.xml")
                .testsToRun("ccc")
                .testCategoriesToInclude("aaa")
                .testCategoriesToExclude("bbb")
                .commandLineOptions("options")
                .environmentVariables("variables")
                .description("description")
                .build();
    }

}