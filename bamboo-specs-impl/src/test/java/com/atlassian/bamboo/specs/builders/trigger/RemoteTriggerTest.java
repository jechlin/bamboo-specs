package com.atlassian.bamboo.specs.builders.trigger;

import com.atlassian.bamboo.specs.api.builders.trigger.RepositoryBasedTrigger.TriggeringRepositoriesType;
import com.atlassian.bamboo.specs.model.trigger.RemoteTriggerProperties;
import org.junit.Test;

import java.util.Collections;

import static org.hamcrest.core.IsEqual.equalTo;
import static org.junit.Assert.assertThat;

public class RemoteTriggerTest {

    @Test
    public void testBuildWithTriggerIPAddresses() throws Exception {
        assertThat(new RemoteTrigger()
                        .description("My remote trigger")
                        .enabled(true)
                        .triggerIPAddresses("1.1.1.1, 2.2.2.2")
                        .build(),
                equalTo(new RemoteTriggerProperties(
                        "My remote trigger",
                        true,
                        TriggeringRepositoriesType.ALL,
                        Collections.emptyList(),
                        "1.1.1.1, 2.2.2.2"
                )));
    }

}