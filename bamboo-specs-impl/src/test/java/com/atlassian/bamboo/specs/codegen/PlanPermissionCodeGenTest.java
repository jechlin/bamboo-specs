package com.atlassian.bamboo.specs.codegen;

import com.atlassian.bamboo.specs.api.builders.permission.PermissionType;
import com.atlassian.bamboo.specs.api.builders.permission.Permissions;
import com.atlassian.bamboo.specs.api.builders.permission.PlanPermissions;
import com.atlassian.bamboo.specs.api.builders.plan.Job;
import com.atlassian.bamboo.specs.api.builders.plan.Plan;
import com.atlassian.bamboo.specs.api.builders.plan.PlanIdentifier;
import com.atlassian.bamboo.specs.api.builders.plan.Stage;
import com.atlassian.bamboo.specs.api.builders.project.Project;
import com.atlassian.bamboo.specs.api.model.permission.PlanPermissionsProperties;
import com.atlassian.bamboo.specs.api.util.EntityPropertiesBuilders;
import com.atlassian.bamboo.specs.builders.task.ScriptTask;
import org.junit.Test;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.stream.Collectors;

import static org.hamcrest.Matchers.equalToIgnoringWhiteSpace;
import static org.junit.Assert.assertThat;

public class PlanPermissionCodeGenTest {

    @Test
    public void testPlanPermissionPlanSpecExport() throws Exception {
        final PlanIdentifier planIdentifier = new PlanIdentifier("PROJ1", "PLAN1");
        final PlanPermissions planPermission = createPlanPermission(planIdentifier);

        final PlanPermissionsProperties planPermissionProperties = EntityPropertiesBuilders.build(planPermission);

        final BambooSpecsGenerator specGenerator = new BambooSpecsGenerator(planPermissionProperties);
        final String code = specGenerator.emitCode();

        final InputStream expectedCodeInputStream = getClass().getClassLoader().getResourceAsStream("com/atlassian/bamboo/specs/codegen/PlanPermissionCodeGenTest-PlanSpec.java");
        final String expectedCode = new BufferedReader(new InputStreamReader(expectedCodeInputStream))
                .lines().collect(Collectors.joining("\n"));
        assertThat(code, equalToIgnoringWhiteSpace(expectedCode));
    }

    @Test
    public void testPlanPermissionPlanSpecExportGroupOnly() throws Exception {
        final PlanIdentifier planIdentifier = new PlanIdentifier("PROJ1", "PLAN1");
        final PlanPermissions planPermission = createPlanPermissionGroupOnly(planIdentifier);

        final PlanPermissionsProperties planPermissionProperties = EntityPropertiesBuilders.build(planPermission);

        final BambooSpecsGenerator specGenerator = new BambooSpecsGenerator(planPermissionProperties);
        final String code = specGenerator.emitCode();

        final InputStream expectedCodeInputStream = getClass().getClassLoader().getResourceAsStream("com/atlassian/bamboo/specs/codegen/PlanPermissionCodeGenTest-PlanSpecOnlyGroups.java");
        final String expectedCode = new BufferedReader(new InputStreamReader(expectedCodeInputStream))
                .lines().collect(Collectors.joining("\n"));
        assertThat(code, equalToIgnoringWhiteSpace(expectedCode));
    }

    @Test
    public void testPlanAndPlanPermissionPlanSpecExport() throws Exception {
        final Plan plan = createPlan();
        final PlanPermissions planPermission = createPlanPermissionGroupOnly(plan.getIdentifier());

        final BambooSpecsGenerator specGenerator = new BambooSpecsGenerator()
                .addEntity("plan", EntityPropertiesBuilders.build(plan))
                .addEntity("planPermission", EntityPropertiesBuilders.build(planPermission));

        final String code = specGenerator.emitCode();

        final InputStream expectedCodeInputStream = getClass().getClassLoader().getResourceAsStream("com/atlassian/bamboo/specs/codegen/PlanPermissionCodeGenTest-PlanSpecPlanAndPlanPermission.java");
        final String expectedCode = new BufferedReader(new InputStreamReader(expectedCodeInputStream))
                .lines().collect(Collectors.joining("\n"));
        assertThat(code, equalToIgnoringWhiteSpace(expectedCode));
    }

    private static PlanPermissions createPlanPermission(PlanIdentifier planIdentifier) {
        final Permissions permission = new Permissions()
                .userPermissions("admin", PermissionType.ADMIN, PermissionType.CLONE, PermissionType.EDIT)
                .groupPermissions("bamboo-admin", PermissionType.ADMIN)
                .loggedInUserPermissions(PermissionType.VIEW)
                .anonymousUserPermissionView();
        return new PlanPermissions(planIdentifier.getProjectKey(), planIdentifier.getPlanKey()).permissions(permission);
    }

    private static PlanPermissions createPlanPermissionGroupOnly(PlanIdentifier planIdentifier) {
        final Permissions permission = new Permissions()
                .groupPermissions("bamboo-admin", PermissionType.ADMIN);
        return new PlanPermissions(planIdentifier.getProjectKey(), planIdentifier.getPlanKey()).permissions(permission);
    }

    private static Project project() {
        return new Project()
                .name("Project Name")
                .key("PRJ");
    }

    private static Plan createPlan() {
        return new Plan(
                project(),
                "Plan Name", "PLANKEY")
                .description("Plan created from (enter repository url of your plan)")
                .stages(
                        new Stage("Stage 1").jobs(
                                new Job("Job Name", "JOBKEY")
                                        .tasks(
                                                scriptTask()
                                        )
                        )
                );
    }

    private static ScriptTask scriptTask() {
        return new ScriptTask()
                .inlineBody("mkdir target; echo 'hello world' > target/console.out")
                .interpreterShell();
    }

}