package com.atlassian.bamboo.specs.codegen.emitters.task;

import com.atlassian.bamboo.specs.api.builders.plan.PlanIdentifier;
import com.atlassian.bamboo.specs.api.codegen.CodeGenerationContext;
import com.atlassian.bamboo.specs.api.util.EntityPropertiesBuilders;
import com.atlassian.bamboo.specs.builders.task.ArtifactItem;
import com.atlassian.bamboo.specs.model.task.BaseSshTaskProperties;
import com.atlassian.bamboo.specs.model.task.ScpTaskProperties;
import org.junit.Before;
import org.junit.Test;

import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertThat;

public class ScpTaskEmitterTest {

    private CodeGenerationContext context;
    private ScpTaskEmitter emitter;

    @Before
    public void setUp() {
        context = new CodeGenerationContext();
        emitter = new ScpTaskEmitter();
    }

    @Test
    public void emitCodeForAllArtifacts() throws Exception {
        final ArtifactItem item = new ArtifactItem()
                .sourcePlan(new PlanIdentifier("ABC", "ONE"))
                .allArtifacts();

        final ScpTaskProperties properties = new ScpTaskProperties("description of the task", true,
                "localhost", "admin", BaseSshTaskProperties.AuthenticationType.KEY_WITH_PASSPHRASE, "null",
                "secret-key", "secret-passphrase", "hostfingerprint", 80,
                EntityPropertiesBuilders.build(item),
                null, false, "remote-dir");

        final String expectedCode =
                "new ScpTask()\n" +
                "    .description(\"description of the task\")\n" +
                "    .host(\"localhost\")\n" +
                "    .username(\"admin\")\n" +
                "    .hostFingerprint(\"hostfingerprint\")\n" +
                "    .port(80)\n" +
                "    .toRemotePath(\"remote-dir\")\n" +
                "    .authenticateWithKeyWithPassphrase(/* PUT YOUR SENSITIVE INFORMATION HERE*/, /* PUT YOUR SENSITIVE INFORMATION HERE*/)\n" +
                "    .fromArtifact(new ArtifactItem()\n" +
                "        .sourcePlan(new PlanIdentifier(\"ABC\", \"ONE\"))\n" +
                "        .allArtifacts())";

        assertThat(emitter.emitCode(context, properties), equalTo(expectedCode));
    }

    @Test
    public void emitCodeForSingleArtifact() throws Exception {
        final ArtifactItem item = new ArtifactItem()
                .sourcePlan(new PlanIdentifier("ABC", "ONE"))
                .artifact("Test Report");

        final ScpTaskProperties properties = new ScpTaskProperties("description of the task", true,
                "localhost", "admin", BaseSshTaskProperties.AuthenticationType.KEY_WITHOUT_PASSPHRASE,
                null, "secret-key", null, "hostfingerprint", 80,
                EntityPropertiesBuilders.build(item),
                null, false, "remote-dir");

        final String expectedCode =
                "new ScpTask()\n" +
                "    .description(\"description of the task\")\n" +
                "    .host(\"localhost\")\n" +
                "    .username(\"admin\")\n" +
                "    .hostFingerprint(\"hostfingerprint\")\n" +
                "    .port(80)\n" +
                "    .toRemotePath(\"remote-dir\")\n" +
                "    .authenticateWithKey(/* PUT YOUR SENSITIVE INFORMATION HERE*/)\n" +
                "    .fromArtifact(new ArtifactItem()\n" +
                "        .sourcePlan(new PlanIdentifier(\"ABC\", \"ONE\"))\n" +
                "        .artifact(\"Test Report\"))";

        assertThat(emitter.emitCode(context, properties), equalTo(expectedCode));
    }

    @Test
    public void emitCodeForArtifactFromCurrentPlan() throws Exception {
        final ArtifactItem item = new ArtifactItem()
                .artifact("Test Report"); // current plan, one artifact

        final ScpTaskProperties properties = new ScpTaskProperties("description of the task", true,
                "localhost", "admin", BaseSshTaskProperties.AuthenticationType.PASSWORD, "secret-password", null, null,
                "hostfingerprint", 80,
                EntityPropertiesBuilders.build(item),
                null, false, "remote-dir");

        // we expect no sourcePlan() method call
        final String expectedCode =
                "new ScpTask()\n" +
                        "    .description(\"description of the task\")\n" +
                        "    .host(\"localhost\")\n" +
                        "    .username(\"admin\")\n" +
                        "    .hostFingerprint(\"hostfingerprint\")\n" +
                        "    .port(80)\n" +
                        "    .toRemotePath(\"remote-dir\")\n" +
                        "    .authenticateWithPassword(/* PUT YOUR SENSITIVE INFORMATION HERE*/)\n" +
                        "    .fromArtifact(new ArtifactItem()\n" +
                        "        .artifact(\"Test Report\"))";

        assertThat(emitter.emitCode(context, properties), equalTo(expectedCode));
    }

    @Test
    public void emitCodeForLocalFiles() throws Exception {
        final ScpTaskProperties properties = new ScpTaskProperties("description of the task", true,
                "localhost", "admin", BaseSshTaskProperties.AuthenticationType.PASSWORD, "secret-password", null, null,
                "hostfingerprint", 80,
                null, "abc.txt,target/abc.jar", false, "remote-dir");

        final String expectedCode =
                "new ScpTask()\n" +
                "    .description(\"description of the task\")\n" +
                "    .host(\"localhost\")\n" +
                "    .username(\"admin\")\n" +
                "    .hostFingerprint(\"hostfingerprint\")\n" +
                "    .port(80)\n" +
                "    .toRemotePath(\"remote-dir\")\n" +
                "    .authenticateWithPassword(/* PUT YOUR SENSITIVE INFORMATION HERE*/)\n" +
                "    .fromLocalPath(\"abc.txt,target/abc.jar\", false)";

        assertThat(emitter.emitCode(context, properties), equalTo(expectedCode));
    }

    @Test
    public void emitCodeForLocalFilesWithAntPatterns() throws Exception {
        final ScpTaskProperties properties = new ScpTaskProperties("description of the task", true,
                "localhost", "admin", BaseSshTaskProperties.AuthenticationType.PASSWORD, "secret-password", null, null,
                "hostfingerprint", 80,
                null, "abc.txt,target/**/*.jar", true, "remote-dir");

        final String expectedCode =
                "new ScpTask()\n" +
                "    .description(\"description of the task\")\n" +
                "    .host(\"localhost\")\n" +
                "    .username(\"admin\")\n" +
                "    .hostFingerprint(\"hostfingerprint\")\n" +
                "    .port(80)\n" +
                "    .toRemotePath(\"remote-dir\")\n" +
                "    .authenticateWithPassword(/* PUT YOUR SENSITIVE INFORMATION HERE*/)\n" +
                "    .fromLocalPath(\"abc.txt,target/**/*.jar\", true)";

        assertThat(emitter.emitCode(context, properties), equalTo(expectedCode));
    }
}