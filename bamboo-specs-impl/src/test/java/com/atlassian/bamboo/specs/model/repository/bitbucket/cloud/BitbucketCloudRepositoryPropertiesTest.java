package com.atlassian.bamboo.specs.model.repository.bitbucket.cloud;

import com.atlassian.bamboo.specs.api.exceptions.PropertiesValidationException;
import com.atlassian.bamboo.specs.builders.repository.bitbucket.cloud.BitbucketCloudRepository;
import com.atlassian.bamboo.specs.builders.repository.git.SshPrivateKeyAuthentication;
import com.atlassian.bamboo.specs.builders.repository.git.UserPasswordAuthentication;
import com.atlassian.bamboo.specs.builders.repository.viewer.BitbucketCloudRepositoryViewer;
import org.junit.Test;

import java.time.Duration;

import static com.atlassian.bamboo.specs.api.util.EntityPropertiesBuilders.build;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.nullValue;
import static org.junit.Assert.assertThat;

public class BitbucketCloudRepositoryPropertiesTest {


    @Test(expected = PropertiesValidationException.class)
    public void testValidation() {
        build(new BitbucketCloudRepository()
                .name("Repository name"));
    }

    @Test(expected = PropertiesValidationException.class)
    public void testValidateBranchShell() {
        build(new BitbucketCloudRepository()
                .name("Repository name")
                .repositorySlug("owner", "repo")
                .branch("a\"b")
        );
    }

    @Test(expected = PropertiesValidationException.class)
    public void testValidateAccAuthNullPassoword() {
        build(new BitbucketCloudRepository()
                .name("Repository name")
                .repositorySlug("owner", "repo")
                .accountAuthentication(new UserPasswordAuthentication("username"))
                .branch("master")
        );
    }

    @Test
    public void testPublicRepository() {
        final BitbucketCloudRepositoryProperties result = build(new BitbucketCloudRepository()
                .name("Repository name")
                .repositorySlug("openclover", "repo")
                .branch("master")
        );

        assertThat(result.getRepositorySlug(), equalTo("openclover/repo"));
        assertThat(result.getAuthenticationProperties(), nullValue());
    }

    @Test
    public void testOK() {
        final String name = "name";
        final String description = "description";
        final String branch = "branch";
        final String owner = "owner";
        final String repository = "repository";
        final String username = "username";
        final String key = "key";
        final String password = "password";

        final BitbucketCloudRepositoryProperties properties = build(new BitbucketCloudRepository()
                .name(name)
                .description(description)
                .branch(branch)
                .lfsEnabled(true)
                .submodulesEnabled(true)
                .fetchWholeRepository(true)
                .remoteAgentCacheEnabled(false)
                .shallowClonesEnabled(false)
                .verboseLogs(true)
                .commandTimeoutInMinutes(500)
                .accountAuthentication(new UserPasswordAuthentication(username).password(password))
                .checkoutAuthentication(new SshPrivateKeyAuthentication(key))
                .repositorySlug(owner, repository));

        assertThat(properties.getRepositoryViewerProperties(), equalTo(build(new BitbucketCloudRepositoryViewer())));
        assertThat(properties.getName(), equalTo(name));
        assertThat(properties.getDescription(), equalTo(description));
        assertThat(properties.getBranch(), equalTo(branch));
        assertThat(properties.isUseShallowClones(), equalTo(false));
        assertThat(properties.isUseRemoteAgentCache(), equalTo(false));
        assertThat(properties.isUseSubmodules(), equalTo(true));
        assertThat(properties.getCommandTimeout(), equalTo(Duration.ofMinutes(500)));
        assertThat(properties.isVerboseLogs(), equalTo(true));
        assertThat(properties.isFetchWholeRepository(), equalTo(true));
        assertThat(properties.isUseLfs(), equalTo(true));
        assertThat(properties.getVcsChangeDetection(), nullValue());
        assertThat(properties.getRepositorySlug(), equalTo(owner + "/" + repository));
        assertThat(properties.getAuthenticationProperties(), equalTo(build(new UserPasswordAuthentication(username).password(password))));
        assertThat(properties.getCheckoutAuthenticationProperties(), equalTo(build(new SshPrivateKeyAuthentication(key))));
    }
}