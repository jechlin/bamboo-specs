package com.atlassian.bamboo.specs.model.task;

import com.atlassian.bamboo.specs.api.exceptions.PropertiesValidationException;
import com.atlassian.bamboo.specs.api.model.BambooKeyProperties;
import com.atlassian.bamboo.specs.api.model.BambooOidProperties;
import com.atlassian.bamboo.specs.api.model.plan.PlanIdentifierProperties;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.nullValue;
import static org.junit.Assert.assertThat;

public class ArtifactItemPropertiesTest {

    @Rule
    public ExpectedException exception = ExpectedException.none();

    private BambooKeyProperties PROJECT = new BambooKeyProperties("PROJECT");
    private BambooKeyProperties PLAN = new BambooKeyProperties("PLAN");
    private BambooOidProperties OID = new BambooOidProperties("oid123");

    @Test
    public void testAllArtifacts() {
        final ArtifactItemProperties properties = new ArtifactItemProperties(
                new PlanIdentifierProperties(PROJECT, PLAN, OID), true, null);

        assertThat(properties.isAllArtifacts(), is(true));
        assertThat(properties.getArtifactName(), nullValue());
        assertThat(properties.getSourcePlan().getKey().getKey(), equalTo(PLAN.getKey()));
    }

    @Test
    public void testOneArtifact() {
        final String ARTIFACT_NAME = "One Artifact";
        final ArtifactItemProperties properties = new ArtifactItemProperties(
                new PlanIdentifierProperties(PROJECT, PLAN, OID), false, ARTIFACT_NAME);

        assertThat(properties.isAllArtifacts(), is(false));
        assertThat(properties.getArtifactName(), equalTo(ARTIFACT_NAME));
        assertThat(properties.getSourcePlan().getKey().getKey(), equalTo(PLAN.getKey()));
    }

    @Test
    public void testArtifactNameIsMissing() {
        exception.expect(PropertiesValidationException.class);
        exception.expectMessage("SCP task / Artifact / Artifact name: Artifact name is required unless all "
                + "artifacts are selected. You have name=null and all artifacts=false");

        new ArtifactItemProperties(
                new PlanIdentifierProperties(PROJECT, PLAN, OID), false, null);
    }
}