package com.atlassian.bamboo.specs.model.task;

import com.atlassian.bamboo.specs.api.exceptions.PropertiesValidationException;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

import static org.hamcrest.core.IsEqual.equalTo;
import static org.junit.Assert.assertThat;

public class ScriptTaskPropertiesTest {

    @Rule
    public ExpectedException exception = ExpectedException.none();

    @Test
    public void testValidationForAllScriptLocation() {
        final Set<ScriptTaskProperties.Location> allLocationTypes = new HashSet<>(Arrays.asList(ScriptTaskProperties.Location.values()));

        allLocationTypes.remove(ScriptTaskProperties.Location.INLINE);
        new ScriptTaskProperties(
                "hello script 1",
                true,
                ScriptTaskProperties.Interpreter.BINSH_OR_CMDEXE,
                ScriptTaskProperties.Location.INLINE,
                "echo Hello World",
                null,
                "arg1 arg2 arg3",
                "SOME_HOME=/home ANOTHER_HOME=/home",
                "work/dir").validate();

        allLocationTypes.remove(ScriptTaskProperties.Location.FILE);
        new ScriptTaskProperties(
                "hello script 1",
                true,
                ScriptTaskProperties.Interpreter.BINSH_OR_CMDEXE,
                ScriptTaskProperties.Location.FILE,
                null,
                "/path/to/script",
                "arg1 arg2 arg3",
                "SOME_HOME=/home ANOTHER_HOME=/home",
                "work/dir").validate();

        for (final ScriptTaskProperties.Location location : allLocationTypes) {
            new ScriptTaskProperties(
                    "hello script 1",
                    true,
                    ScriptTaskProperties.Interpreter.BINSH_OR_CMDEXE,
                    location,
                    null,
                    null,
                    "arg1 arg2 arg3",
                    "SOME_HOME=/home ANOTHER_HOME=/home",
                    "work/dir").validate();
        }
    }

    @Test
    public void testAtlassianId() {
        assertThat(new ScriptTaskProperties(
                        "hello script 1",
                        true,
                        ScriptTaskProperties.Interpreter.BINSH_OR_CMDEXE,
                        ScriptTaskProperties.Location.INLINE,
                        "echo Hello World",
                        null,
                        "arg1 arg2 arg3",
                        "SOME_HOME=/home ANOTHER_HOME=/home",
                        "work/dir").getAtlassianPlugin().getCompleteModuleKey(),
                equalTo("com.atlassian.bamboo.plugins.scripttask:task.builder.script"));
    }

    @Test
    public void testValidationWhenLocationFileAndBodySet() {
        exception.expect(PropertiesValidationException.class);
        exception.expectMessage(String.format("Script body cannot be set when location is set to %s", ScriptTaskProperties.Location.FILE));
        new ScriptTaskProperties(
                "hello script 1",
                true,
                ScriptTaskProperties.Interpreter.BINSH_OR_CMDEXE,
                ScriptTaskProperties.Location.FILE,
                "echo Hello World",
                "/path/to/script",
                "arg1 arg2 arg3",
                "SOME_HOME=/home ANOTHER_HOME=/home",
                "work/dir").validate();
    }

    @Test
    public void testValidationWhenLocationFileAndPathNotSet() {
        exception.expect(PropertiesValidationException.class);
        exception.expectMessage(String.format("Script path cannot be empty when location is set to %s", ScriptTaskProperties.Location.FILE));
        new ScriptTaskProperties(
                "hello script 1",
                true,
                ScriptTaskProperties.Interpreter.BINSH_OR_CMDEXE,
                ScriptTaskProperties.Location.FILE,
                null,
                null,
                "arg1 arg2 arg3",
                "SOME_HOME=/home ANOTHER_HOME=/home",
                "work/dir").validate();
    }

    @Test
    public void testValidationWhenLocationInlineAndPathSet() {
        exception.expect(PropertiesValidationException.class);
        exception.expectMessage(String.format("Script path cannot be set when location is set to %s", ScriptTaskProperties.Location.INLINE));
        new ScriptTaskProperties(
                "hello script 1",
                true,
                ScriptTaskProperties.Interpreter.BINSH_OR_CMDEXE,
                ScriptTaskProperties.Location.INLINE,
                "echo Hello World",
                "/path/to/script",
                "arg1 arg2 arg3",
                "SOME_HOME=/home ANOTHER_HOME=/home",
                "work/dir").validate();
    }

    @Test
    public void testValidationWhenLocationInlineAndBodyNotSet() {
        exception.expect(PropertiesValidationException.class);
        exception.expectMessage(String.format("Script body cannot be empty when location is set to %s", ScriptTaskProperties.Location.INLINE));
        new ScriptTaskProperties(
                "hello script 1",
                true,
                ScriptTaskProperties.Interpreter.BINSH_OR_CMDEXE,
                ScriptTaskProperties.Location.INLINE,
                null,
                null,
                "arg1 arg2 arg3",
                "SOME_HOME=/home ANOTHER_HOME=/home",
                "work/dir").validate();
    }

    @Test
    public void testDefaultInterpreterIsShell() {
        assertThat(new ScriptTaskProperties(
                        "hello script 1",
                        true,
                        null,
                        ScriptTaskProperties.Location.INLINE,
                        "echo Hello World",
                        null,
                        "arg1 arg2 arg3",
                        "SOME_HOME=/home ANOTHER_HOME=/home",
                        "work/dir").getInterpreter(),
                equalTo(ScriptTaskProperties.Interpreter.SHELL));
    }

    @Test
    public void testDefaultLocationIsInline() {
        assertThat(new ScriptTaskProperties(
                        "hello script 1",
                        true,
                        null,
                        null,
                        "echo Hello World",
                        null,
                        "arg1 arg2 arg3",
                        "SOME_HOME=/home ANOTHER_HOME=/home",
                        "work/dir").getLocation(),
                equalTo(ScriptTaskProperties.Location.INLINE));
    }

}