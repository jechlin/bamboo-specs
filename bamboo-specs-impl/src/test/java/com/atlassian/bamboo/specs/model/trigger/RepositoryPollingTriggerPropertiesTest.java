package com.atlassian.bamboo.specs.model.trigger;

import com.atlassian.bamboo.specs.api.builders.repository.VcsRepositoryIdentifier;
import com.atlassian.bamboo.specs.api.exceptions.PropertiesValidationException;
import com.atlassian.bamboo.specs.api.util.EntityPropertiesBuilders;
import com.atlassian.bamboo.specs.builders.trigger.RepositoryPollingTrigger;
import junitparams.JUnitParamsRunner;
import junitparams.Parameters;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;

import java.time.DayOfWeek;
import java.time.Duration;
import java.time.LocalTime;
import java.util.concurrent.TimeUnit;

import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;

@RunWith(JUnitParamsRunner.class)
public class RepositoryPollingTriggerPropertiesTest {
    @Rule
    public ExpectedException expectedException = ExpectedException.none();

    private static final String NAME = "name";

    @Test
    public void testSuccessfulCreation() throws Exception {
        RepositoryPollingTriggerProperties trigger = EntityPropertiesBuilders.build(repositoryPollingTrigger()
                .pollWithCronExpression("0 0 0 ? * *"));

        assertThat(trigger.getCronExpression(), equalTo("0 0 0 ? * *"));
        assertThat(trigger.getPollType(), equalTo(RepositoryPollingTriggerProperties.PollType.CRON));
    }

    @Test
    public void testInvalidCronExpression() throws Exception {
        expectedException.expect(PropertiesValidationException.class);

        RepositoryPollingTriggerProperties trigger = EntityPropertiesBuilders.build(repositoryPollingTrigger()
                .pollWithCronExpression("not valid cron expression"));
    }

    @Test
    public void testCreateWithoutRepositories() throws Exception {
        RepositoryPollingTriggerProperties trigger = EntityPropertiesBuilders.build(repositoryPollingTrigger()
                .allAvailableTriggeringRepositories()
                .pollEvery(30, TimeUnit.SECONDS));

        assertTrue(trigger.getSelectedTriggeringRepositories().isEmpty());
    }

    @Parameters(method = "pollEveryData")
    @Test
    public void testPollEvery(boolean shouldFail, TimeUnit timeUnit, Duration expectedDuration) throws Exception {
        if (shouldFail) {
            expectedException.expect(PropertiesValidationException.class);
        }

        RepositoryPollingTriggerProperties trigger = EntityPropertiesBuilders.build(repositoryPollingTrigger()
                .pollEvery(30, timeUnit));

        assertThat(trigger.getPollingPeriod(), equalTo(expectedDuration));
        assertThat(trigger.getPollType(), equalTo(RepositoryPollingTriggerProperties.PollType.PERIOD));
    }


    @SuppressWarnings("unused") //used by @Parameters test
    private Object[] pollEveryData() {
        return new Object[]
                {
                        new Object[]{
                                true, null, null
                        },
                        new Object[]{
                                true, TimeUnit.MICROSECONDS, null
                        },
                        new Object[]{
                                true, TimeUnit.MILLISECONDS, null
                        },
                        new Object[]{
                                false, TimeUnit.SECONDS, Duration.ofSeconds(30)
                        },
                        new Object[]{
                                false, TimeUnit.MINUTES, Duration.ofMinutes(30)
                        },
                        new Object[]{
                                false, TimeUnit.HOURS, Duration.ofHours(30)
                        },
                        new Object[]{
                                false, TimeUnit.DAYS, Duration.ofDays(30)
                        }
                };
    }

    @Test
    public void testPollOnceDaily() throws Exception {
        RepositoryPollingTriggerProperties trigger = EntityPropertiesBuilders.build(repositoryPollingTrigger()
                .pollOnceDaily(LocalTime.of(15, 13)));

        assertThat(trigger.getCronExpression(), equalTo("0 13 15 ? * *"));
        assertThat(trigger.getPollType(), equalTo(RepositoryPollingTriggerProperties.PollType.CRON));
    }

    @Test
    public void testPollWeekly() throws Exception {
        RepositoryPollingTriggerProperties trigger = EntityPropertiesBuilders.build(repositoryPollingTrigger()
                .pollWeekly(LocalTime.of(14, 13), DayOfWeek.SUNDAY));

        assertThat(trigger.getCronExpression(), equalTo("0 13 14 ? * 1"));
        assertThat(trigger.getPollType(), equalTo(RepositoryPollingTriggerProperties.PollType.CRON));
    }

    @Test
    public void testPollWeeklyMoreDays() throws Exception {
        RepositoryPollingTriggerProperties trigger = EntityPropertiesBuilders.build(repositoryPollingTrigger()
                .pollWeekly(LocalTime.of(8, 8), DayOfWeek.FRIDAY, DayOfWeek.FRIDAY, DayOfWeek.SATURDAY, DayOfWeek.WEDNESDAY));

        assertThat(trigger.getCronExpression(), equalTo("0 8 8 ? * 6,6,7,4"));
        assertThat(trigger.getPollType(), equalTo(RepositoryPollingTriggerProperties.PollType.CRON));
    }

    @Test
    public void testPollMonthly() throws Exception {
        RepositoryPollingTriggerProperties trigger = EntityPropertiesBuilders.build(repositoryPollingTrigger()
                .pollMonthly(LocalTime.of(13, 0), 11));

        assertThat(trigger.getCronExpression(), equalTo("0 0 13 11 * ?"));
        assertThat(trigger.getPollType(), equalTo(RepositoryPollingTriggerProperties.PollType.CRON));
    }

    private RepositoryPollingTrigger repositoryPollingTrigger() {
        return new RepositoryPollingTrigger()
                .name(NAME)
                .selectedTriggeringRepositories(new VcsRepositoryIdentifier("repo"));
    }
}
