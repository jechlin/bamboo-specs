package com.atlassian.bamboo.specs.maven;

import org.apache.maven.plugin.logging.Log;

import java.io.IOException;
import java.nio.file.FileVisitResult;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.SimpleFileVisitor;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.ArrayList;
import java.util.Collection;

public final class BambooSpecsFileScanner {
    private BambooSpecsFileScanner() {
    }

    public static Collection<Path> getClassFiles(final Log log, final Path root) throws IOException {
        final Collection<Path> classFiles = new ArrayList<>();
        Files.walkFileTree(root, new SimpleFileVisitor<Path>() {
            @Override
            public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) throws IOException {
                if (file.toString().endsWith(".class")) {
                    log.debug("Adding " + file + " to scan candidates");
                    classFiles.add(root.relativize(file));
                }
                return super.visitFile(file, attrs);
            }
        });
        return classFiles;
    }
}
