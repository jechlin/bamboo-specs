package com.atlassian.bamboo.specs.maven;

import java.security.AccessControlException;
import java.security.Permission;
import java.util.concurrent.atomic.AtomicReference;

public class BambooSpecsSecurityManager extends SecurityManager {
    private static final AtomicReference<Thread> HIGH_PRIVILEGE_THREAD = new AtomicReference<>();

    private static final SecurityManager SEC = new SecurityManager();

    @Override
    public void checkPermission(final Permission perm) {
        if (isLowPriviliegeThread()) {
            try {
                SEC.checkPermission(perm);
            } catch (final AccessControlException e) {
                throw e;
            }
        }
    }

    @Override
    public void checkPermission(final Permission perm, final Object context) {
        if (isLowPriviliegeThread()) {
            super.checkPermission(perm, context);
        }
    }

    private static boolean isLowPriviliegeThread() {
        return !Thread.currentThread().equals(HIGH_PRIVILEGE_THREAD.get());
    }

    public static void addHighPrivilegeThread() {
        if (!HIGH_PRIVILEGE_THREAD.compareAndSet(null, Thread.currentThread())) {
            throw new IllegalStateException("Cannot nest secure method invocations");
        }
    }

    public static void removeHighPrivilegeThread() {
        HIGH_PRIVILEGE_THREAD.set(null);
    }
}
