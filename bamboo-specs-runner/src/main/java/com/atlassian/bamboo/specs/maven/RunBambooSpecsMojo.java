package com.atlassian.bamboo.specs.maven;

import com.atlassian.bamboo.specs.api.rsbs.RunnerSettings;
import com.atlassian.bamboo.specs.api.rsbs.RepositoryStoredSpecsData;
import com.atlassian.bamboo.specs.maven.sandbox.FileToBambooSpecsMapper;
import com.atlassian.bamboo.specs.maven.sandbox.SecureMethodInvoker;
import com.atlassian.bamboo.specs.maven.sandbox.SpecsRunner;
import org.apache.maven.artifact.DependencyResolutionRequiredException;
import org.apache.maven.plugin.AbstractMojo;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugins.annotations.Execute;
import org.apache.maven.plugins.annotations.LifecyclePhase;
import org.apache.maven.plugins.annotations.Mojo;
import org.apache.maven.plugins.annotations.Parameter;
import org.apache.maven.plugins.annotations.ResolutionScope;
import org.apache.maven.project.MavenProject;

import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLClassLoader;
import java.nio.file.Path;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.function.Supplier;
import java.util.stream.Collectors;

@Mojo(
        name = "run",
        defaultPhase = LifecyclePhase.NONE,
        requiresDependencyResolution = ResolutionScope.RUNTIME,
        requiresOnline = true

)
@Execute(phase = LifecyclePhase.PACKAGE)
public class RunBambooSpecsMojo extends AbstractMojo {
    @Parameter(defaultValue = "${project}", required = true, readonly = true)
    private MavenProject project;

    @Parameter(defaultValue = "${project.build.outputDirectory}", property = "specsDir", required = true)
    private File outputDirectory;

    @Parameter(defaultValue = "false")
    private boolean skip;

    @Parameter(property = "specs.yamlDir")
    private File yamlDir;

    @Parameter(property = "specs.useRest", defaultValue = "true")
    private boolean useRest;

    //Repository-stored Bamboo Specs
    @Parameter(property = "specs.rs.vcsLocationId")
    private Integer rsbsVcsLocationId;

    @Parameter(property = "specs.rs.branch")
    private String rsbsBranch;

    @Parameter(property = "specs.rs.revision")
    private String rsbsRevision;

    @Override
    public void execute() throws MojoExecutionException {
        if (skip) {
            getLog().debug("Skipping plan execution as the 'skip' property is set to true");
            return;
        }

        getLog().info("Scanning " + outputDirectory + " for classes annotated with Bamboo plan annotation.");

        final URLClassLoader classLoader = createClassLoader();

        final Collection<Path> classFiles = getClassFiles(outputDirectory.toPath());

        final SpecsRunner specsRunner = new SpecsRunner(getLog(), classFiles, new FileToBambooSpecsMapper(getLog(), classLoader));

        RunnerSettings.setRestEnabled(useRest);
        if (yamlDir!=null) {
            RunnerSettings.setYamlDir(yamlDir.toPath());
        }

        if (rsbsBranch!=null || rsbsRevision!=null || rsbsVcsLocationId!=null) {
            RunnerSettings.setRepositoryStoredSpecsData(new RepositoryStoredSpecsData(rsbsVcsLocationId, rsbsBranch, rsbsRevision));
        }

        specsRunner.runSpecs();
    }

    private Collection<Path> getClassFiles(final Path root) throws MojoExecutionException {
        final Collection<Path> classFiles;
        try {
            classFiles = BambooSpecsFileScanner.getClassFiles(getLog(), root);
        } catch (final IOException e) {
            throw new MojoExecutionException("Unable to find class files in " + outputDirectory, e);
        }
        return classFiles;
    }

    private URLClassLoader createClassLoader() throws MojoExecutionException {
        final URLClassLoader classLoader;
        try {
            final URL[] runtimeClasspath = project.getRuntimeClasspathElements().stream()
                    .map(this::newUrl)
                    .toArray(URL[]::new);
            classLoader = new URLClassLoader(runtimeClasspath, Thread.currentThread().getContextClassLoader());
        } catch (final DependencyResolutionRequiredException e) {
            throw new MojoExecutionException("Unable to find dependency", e);
        }
        return classLoader;
    }

    private URL newUrl(final String file) {
        try {
            return new File(file).toURI().toURL();
        } catch (final MalformedURLException e) {
            throw new RuntimeException("Unable to construct classloader", e);
        }
    }

    public static void main(String[] args) {
        // System.setSecurityManager(new AppletSecurity());
        //
        final Supplier<Object> hostileCode = new Supplier<Object>() {
            @Override
            public Object get() {
                final List<Optional<Object>> objects = Collections.emptyList();
                objects.stream().filter(Optional::isPresent).collect(Collectors.toList());
                return null;
            }
        };

        SecureMethodInvoker.invoke(hostileCode);
    }
}
