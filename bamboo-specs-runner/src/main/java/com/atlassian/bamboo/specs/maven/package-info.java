/**
 * Contains classes that allow Bamboo specs to be run with maven.
 */
package com.atlassian.bamboo.specs.maven;
