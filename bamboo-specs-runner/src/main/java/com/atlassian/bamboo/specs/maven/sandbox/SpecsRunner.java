package com.atlassian.bamboo.specs.maven.sandbox;

import org.apache.maven.plugin.logging.Log;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.nio.file.Path;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

public class SpecsRunner {
    private final Log log;
    private final Collection<Path> classFiles;
    private final FileToBambooSpecsMapper fileToBambooSpecsMapper;

    public SpecsRunner(final Log log, final Collection<Path> classFiles, final FileToBambooSpecsMapper fileToBambooSpecsMapper) {
        this.log = log;
        this.classFiles = classFiles;
        this.fileToBambooSpecsMapper = fileToBambooSpecsMapper;
    }

    public Map<Class<?>, Object> runSpecs() {
        return SecureMethodInvoker.invoke(this::runSpecsUnderLowPrivilege);
    }

    @NotNull
    private Map<Class<?>, Object> runSpecsUnderLowPrivilege() {
        final List<Class<?>> bambooSpecClasses = classFiles.stream()
                .map(fileToBambooSpecsMapper)
                .filter(Optional::isPresent)
                .map(Optional::get)
                .collect(Collectors.toList());

        for (final Class<?> bambooSpecClass : bambooSpecClasses) {
            log.info("Running " + bambooSpecClass);
            callMainMethod(bambooSpecClass);
        }

        return Collections.emptyMap();
    }

    @Nullable
    private Object callMainMethod(final Class<?> aClass) {
        final Method mainMethod = getMainMethod(aClass);
        if (mainMethod == null) {
            log.warn("No public static void main() method in " + aClass);
            return null;
        }

        final Object[] args = {
                new String[0]
        };

        try {
            return mainMethod.invoke(null, args);
        } catch (final InvocationTargetException | IllegalAccessException e) {
            throw new RuntimeException(e);
        }
    }

    private static Method getMainMethod(final Class<?> aClass) {
        try {
            return aClass.getMethod("main", String[].class);
        } catch (final NoSuchMethodException ignored) {
            return null;
        }
    }
}
