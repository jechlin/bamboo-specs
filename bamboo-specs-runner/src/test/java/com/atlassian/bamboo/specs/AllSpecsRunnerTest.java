package com.atlassian.bamboo.specs;

import com.atlassian.bamboo.specs.maven.BambooSpecsFileScanner;
import com.atlassian.bamboo.specs.maven.sandbox.FileToBambooSpecsMapper;
import com.atlassian.bamboo.specs.maven.sandbox.SecureMethodInvoker;
import com.atlassian.bamboo.specs.maven.sandbox.SpecsRunner;
import org.apache.maven.plugin.logging.SystemStreamLog;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Collection;
import java.util.concurrent.atomic.AtomicReference;

import static org.junit.jupiter.api.Assertions.assertThrows;

class AllSpecsRunnerTest {
    private static final SystemStreamLog LOG = new SystemStreamLog();
    static ThreadLocal<AtomicReference<Class<?>>> specRunStartedTl = new InheritableThreadLocal<>();
    static ThreadLocal<AtomicReference<Class<?>>> specRunEndedTl = new InheritableThreadLocal<>();

    @Test
    void runAllBambooSpecs() throws IOException {
        specRunStartedTl.set(new AtomicReference<>());
        specRunEndedTl.set(new AtomicReference<>());

        final Path targetDir = Paths.get("/opt/dev/src/atlassian/BambooSpecs/specsrunner/target");
        final Path classPathRoot = targetDir.resolve("test-classes");

        final Collection<Path> classFiles = BambooSpecsFileScanner.getClassFiles(LOG, classPathRoot);

        final SpecsRunner specsRunner = new SpecsRunner(LOG, classFiles, new FileToBambooSpecsMapper(LOG, getClass().getClassLoader()));

        assertThrows(SecureMethodInvoker.SecureMethodInvocationException.class, specsRunner::runSpecs);
    }
}
