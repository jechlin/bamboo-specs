package com.atlassian.bamboo.specs;

import com.atlassian.bamboo.specs.maven.sandbox.FileToBambooSpecsMapper;
import com.atlassian.bamboo.specs.maven.sandbox.SecureMethodInvoker;
import com.atlassian.bamboo.specs.maven.sandbox.SpecsRunner;
import org.apache.maven.plugin.logging.SystemStreamLog;
import org.hamcrest.CoreMatchers;
import org.jetbrains.annotations.NotNull;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Collections;
import java.util.concurrent.atomic.AtomicReference;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;

class SpecsRunnerTest {
    static ThreadLocal<AtomicReference<Class<?>>> specRunStartedTl = new InheritableThreadLocal<>();
    static ThreadLocal<AtomicReference<Class<?>>> specRunEndedTl = new InheritableThreadLocal<>();
    private static final SystemStreamLog LOG = new SystemStreamLog();

    @Test
    void runsSimpleBambooSpecs() throws IOException, URISyntaxException {
        specRunStartedTl.set(new AtomicReference<>());
        specRunEndedTl.set(new AtomicReference<>());

        final Class<TestSimpleSpec> specClass = TestSimpleSpec.class;

        final SpecsRunner specsRunner = newSpecRunner(specClass);
        specsRunner.runSpecs();

        assertThat(specRunStartedTl.get().get(), equalTo(specClass));
        assertThat(specRunEndedTl.get().get(), equalTo(specClass));
    }

    @Test
    void runHostileBambooSpecs() throws IOException, URISyntaxException {
        specRunStartedTl.set(new AtomicReference<>());
        specRunEndedTl.set(new AtomicReference<>());

        final Class<TestHostileSpec> specClass = TestHostileSpec.class;

        final SpecsRunner specsRunner = newSpecRunner(specClass);

        assertThrows(SecureMethodInvoker.SecureMethodInvocationException.class, specsRunner::runSpecs);

        assertThat(specRunStartedTl.get().get(), equalTo(specClass));
        assertThat(specRunEndedTl.get().get(), CoreMatchers.nullValue());

        final Path tempFile2 = Files.createTempFile(getClass().getName(), getClass().getName());
        Files.delete(tempFile2);
    }

    @NotNull
    private SpecsRunner newSpecRunner(final Class<?> specClass) throws URISyntaxException {
        final URI specClassUri = specClass.getProtectionDomain().getCodeSource().getLocation().toURI();

        final Path bambooSpecClassFile = Paths.get(specClassUri);

        return new SpecsRunner(LOG, Collections.singleton(bambooSpecClassFile), new FileToBambooSpecsMapper(LOG, getClass().getClassLoader()));
    }
}
